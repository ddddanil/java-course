package lab2;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

class MatrixTest {
    static final Random rand = new Random();
    Matrix matrix;

    @BeforeEach
    void makeMatrix() {
        int size = rand.nextInt(10) + 1; // Between 1 and 11
        matrix = new Matrix(size, Integer::sum);
    }

    @RepeatedTest(100)
    void checkSetIndex() {
        int N = matrix.size();
        int row = rand.nextInt(N);
        int col = rand.nextInt(N);
        double val = rand.nextDouble();

        matrix.setMatrix(row, col, val);

        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (i == row && j == col) {
                    assertEquals(matrix.getMatrix(i, j), val);
                }
                else {
                    assertEquals(matrix.getMatrix(i, j), i + j);
                }
            }
        }
    }

    @RepeatedTest(20)
    void checkRowOutOfBounds() {
        int invalid_row = rand.nextInt(10) + matrix.size();
        int valid_col = rand.nextInt(matrix.size());
        Exception e = assertThrows(LabException.class, () -> matrix.getMatrix(invalid_row, valid_col));
        assertTrue(e.getMessage().contains("Row"));
    }

    @RepeatedTest(20)
    void checkColOutOfBounds() {
        int invalid_col = rand.nextInt(10) + matrix.size();
        int valid_row = rand.nextInt(matrix.size());
        Exception e = assertThrows(LabException.class, () -> matrix.getMatrix(valid_row, invalid_col));
        assertTrue(e.getMessage().contains("Column"));
    }

    @Test
    void testRotate90CW() {
        Matrix rotated = matrix.rotate90CW();
        int last = rotated.size() - 1;
        assertAll("corners",
                  () -> assertEquals(matrix.getMatrix(0, 0), rotated.getMatrix(0, last)),
                  () -> assertEquals(matrix.getMatrix(0, last), rotated.getMatrix(last, last)),
                  () -> assertEquals(matrix.getMatrix(last, 0), rotated.getMatrix(0, 0)),
                  () -> assertEquals(matrix.getMatrix(last, last), rotated.getMatrix(last, 0)));
    }
}
