package hw1;

import hw1.md.Element;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.Random;

import static hw1.md.Element.*;
import static hw1.md.Element.Text;
import static hw1.md.Element.Emphasis;
import static hw1.md.Element.Paragraph;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarkdownTest {
    static Random rand;
    StringBuilder builder;

    @BeforeAll
    static void initRandom() {
        rand = new Random();
    }

    @BeforeEach
    void newBuilder() {
        builder = new StringBuilder();
    }

    static String RandomAlphanum(int length) {
        return rand.ints('0', 'z')
                .filter((c) -> Character.isAlphabetic(c) || Character.isDigit(c))
                .limit(length)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    @Test
    void task() {
        var markdown = Paragraph(List.of(
                Strong(List.of(
                        Text("1"),
                        Strikeout(List.of(
                                Text("2"),
                                Emphasis(List.of(
                                        Text("3"),
                                        Text("4")
                                )),
                                Text("5")
                        )),
                        Text("6")
                ))
        ));
        var builder = new StringBuilder();
        markdown.toMarkdown(builder);
        var actual = "__1~2*34*5~6__";
        assertEquals(builder.toString(), actual);
    }

    @Test
    void complex() {
        var md = Paragraph(List.of(
                Heading1("Sample"),
                Heading3(Strong("Lorem")),
                Paragraph(List.of(
                        Text("Lorem ipsum dolor sit amet, "),
                        Emphasis("consectetur adipiscing elit."),
                        Text(" Nunc tempor mauris vel congue condimentum.\n")
                )),
                Heading3(List.of(Text("ipsum "), Emphasis("dolor"))),
                Paragraph(List.of(
                        Text("Home directory: "), Code("/home/${HOME}/"), Text("\n")
                )),
                Heading4("sit amet."),
                CodeBlock("""
                        subgenre :: Traversal' ID3v1Tag Text
                        subgenre = _ID3v1E . #genre `failing` _ID3v12 . #subgenre""")
        ));
        var actual = """
                # Sample
                ### __Lorem__
                Lorem ipsum dolor sit amet, *consectetur adipiscing elit.* Nunc tempor mauris vel congue condimentum.
                ### ipsum *dolor*
                Home directory: `/home/${HOME}/`
                #### sit amet.
                ```
                subgenre :: Traversal' ID3v1Tag Text
                subgenre = _ID3v1E . #genre `failing` _ID3v12 . #subgenre
                ```""";
        md.toMarkdown(builder);
        assertEquals(builder.toString(), actual);
    }

    @Nested
    class Surrounds {
        @RepeatedTest(10)
        void text() {
            var len = rand.nextInt(10);
            var str = RandomAlphanum(len);
            var md = Element.Text(str);
            md.toMarkdown(builder);
            assertEquals(builder.toString(), str);
        }

        @RepeatedTest(10)
        void strong() {
            var len = rand.nextInt(10);
            var str = RandomAlphanum(len);
            var md = Element.Strong(str);
            md.toMarkdown(builder);
            var actual = "__" + str + "__";
            assertEquals(builder.toString(), actual);
        }
    }
}
