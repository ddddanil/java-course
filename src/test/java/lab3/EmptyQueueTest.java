package lab3;

import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EmptyQueueTest extends ArrayQueueTest {
    @Test
    void isEmptyFull() {
        assertTrue(queue.isEmpty());
        assertFalse(queue.isFull());
    }

    @Test
    void size() {
        Assertions.assertEquals(queue.size(), 0);
    }

    @RepeatedTest(10)
    void tryGet() {
        assertThrows(NoSuchElementException.class, () -> queue.remove());
        assertThrows(NoSuchElementException.class, () -> queue.element());
        assertNull(queue.poll());
        assertNull(queue.peek());
    }

    @RepeatedTest(10)
    void toArray() {
        var arr = queue.toArray(new Integer[0]);
        assertEquals(Arrays.stream(arr).count(), 0);
    }
}
