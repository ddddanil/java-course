package lab3;

import lab3.fungi.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FungiQueueTest {
    static final Agaricales agaricales = new Agaricales();
    static final AmanitaMuscaria amanitaMuscaria = new AmanitaMuscaria();
    static final AmanitaPhalloides amanitaPhalloides = new AmanitaPhalloides();
    static final LentinulaEdodes lentinulaEdodes = new LentinulaEdodes();
    ArrayQueue<Agaricales> queue;

    @BeforeEach
    void fillQueue() {
        queue = new ArrayQueue<>(10);
        queue.addAll(Stream.of(amanitaMuscaria, agaricales, amanitaPhalloides, lentinulaEdodes).toList());
    }

    @Test
    void produceAmanita() {
        var amanita = queue.produce(Amanita.class);
        var expected = Stream.of(amanitaMuscaria, amanitaPhalloides);
        Assertions.assertArrayEquals(amanita.toArray(), expected.toArray());
    }

    @Test
    void consumeAmanita() {
        var amanitaPar = queue.consume(Amanita.class);
        var expected = Stream.of(agaricales).toList();
        Assertions.assertArrayEquals(amanitaPar.toArray(), expected.toArray());
    }
}
