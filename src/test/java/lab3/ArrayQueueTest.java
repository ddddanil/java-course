package lab3;

import org.junit.jupiter.api.BeforeEach;

import java.util.Random;

class ArrayQueueTest {
    ArrayQueue<Integer> queue;
    final int capacity = 10;
    static final Random rand = new Random();

    private static void addOffset(ArrayQueue<Integer> queue, int offset) {
        for(int i = 0; i < offset; ++i) {
            queue.add(null);
            queue.remove();
        }
    }

    @BeforeEach
    void initQueueWithOffset() {
        queue = new ArrayQueue<>(capacity);
        var offset = rand.nextInt(capacity);
        addOffset(queue, offset);
    }
}
