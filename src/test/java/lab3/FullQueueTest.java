package lab3;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class FullQueueTest extends ArrayQueueTest {
    @BeforeEach
    void fillQueue() {
        for (int i = 0; i < capacity; ++i) {
            assertTrue(queue.add(i));
        }
    }

    @Test
    void isEmptyFull() {
        assertFalse(queue.isEmpty());
        assertTrue(queue.isFull());
    }

    @RepeatedTest(10)
    void size() {
        assertEquals(queue.size(), capacity);
    }

    @RepeatedTest(10)
    void tryPut() {
        assertThrows(IllegalStateException.class, () -> queue.add(11));
        assertFalse(queue.offer(11));
    }

    @RepeatedTest(10)
    void toArray() {
        var arr = queue.toArray(new Integer[0]);
        Integer[] expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertArrayEquals(arr, expected);
    }
}
