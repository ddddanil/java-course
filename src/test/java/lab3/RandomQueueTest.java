package lab3;

import org.junit.jupiter.api.*;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class RandomQueueTest extends ArrayQueueTest {
    int len;

    @BeforeEach
    void fillQueue() {
        len = ArrayQueueTest.rand.nextInt(8) + 1;
        for (int i = 0; i < len; ++i) {
            assertTrue(queue.add(ArrayQueueTest.rand.nextInt()));
        }
    }

    @Test
    void isEmptyFull() {
        assertFalse(queue.isEmpty());
        assertFalse(queue.isFull());
    }

    @RepeatedTest(5)
    void size() {
        assertEquals(queue.size(), len);
    }

    @RepeatedTest(10)
    void firstElem() {
        var firstElem = assertDoesNotThrow(() -> queue.element());
        var firstPeek = queue.peek();
        assertNotNull(firstElem);
        assertNotNull(firstPeek);
        assertEquals(firstElem, firstPeek);
    }

    @RepeatedTest(10)
    void consumeQueueViaRemove() {
        for (int i = 0; i < len; ++i) {
            assertDoesNotThrow(() -> queue.remove());
        }
        assertThrows(NoSuchElementException.class, () -> queue.remove());
    }

    @RepeatedTest(10)
    void consumeQueueViaPoll() {
        for (int i = 0; i < len; ++i) {
            var result = queue.poll();
            assertNotNull(result);
        }
        assertNull(queue.poll());
    }

    @RepeatedTest(5)
    void manyRollovers() {
        for (int i = 0; i < 100; ++i) {
            assertTrue(queue.add(i));
            assertDoesNotThrow(() -> queue.remove());
        }
    }
}
