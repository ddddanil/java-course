package lab5;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class StreamMethodsTest {
    @Test
    public void getAverageTest() {
        var test = new ArrayList<Double>();
        test.add(1.0);
        test.add(4.0);
        test.add(10.0);

        assertEquals(StreamMethods.getAverage(test.stream()), 5.0);
    }

    @Test
    public void changeRegTest() {
        var test = new ArrayList<String>();
        test.add("Test1");
        test.add("tESt2");
        test.add("new_test3");

        String[] actual = {"__new__TEST1", "__new__TEST2", "__new__NEW_TEST3"};

        assertArrayEquals(StreamMethods.changeReg(test.stream()).toArray(), actual);
    }

    @Test
    public void uniqueSquaredTest() {
        var test = new ArrayList<Integer>();
        test.add(5);
        test.add(1);
        test.add(3);
        test.add(3);
        test.add(1);

        Integer[] actual = {1, 9, 25};

        assertArrayEquals(StreamMethods.uniqueSquared(test.stream()).toArray(), actual);
    }

    @Test
    public void filterStartCharTest() {
        var test = new ArrayList<String>();
        test.add("Test2");
        test.add("Test1");
        test.add("new_test3");

        String[] actual = {"Test1", "Test2"};

        assertArrayEquals(StreamMethods.filterStartChar(test.stream(), 'T').toArray(), actual);
    }

    @Test
    public void lastElemTest() {
        var test = new ArrayList<Integer>();

        assertThrows(NoSuchElementException.class, () -> StreamMethods.lastElem(test.stream()));

        test.add(5);
        test.add(1);
        test.add(3);
        test.add(3);
        test.add(1);

        assertEquals(StreamMethods.lastElem(test.stream()), 1);
    }

    @Test
    public void sumEvenTest() {
        var test = new ArrayList<Integer>();
        test.add(4);
        test.add(1);
        test.add(8);
        test.add(3);
        test.add(1);

        assertEquals(StreamMethods.sumEven(test.stream()), 12);
    }

    @Test
    public void createMapTest() {
        var test = new ArrayList<String>();
        test.add("Test2");
        test.add("tEST1");
        test.add("new_test3");

        var res = new HashMap<Character, String>();
        res.put('T', "est2");
        res.put('t', "EST1");
        res.put('n', "ew_test3");

        assertEquals(StreamMethods.createMap(test.stream()), res);
    }
}
