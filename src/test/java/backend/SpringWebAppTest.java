package backend;

import backend.dto.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.assertj.core.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.event.annotation.BeforeTestExecution;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Random;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringWebAppTest {
    static final Random rand = new Random();
    @LocalServerPort
    private int port;

    String randomSubstring(String str) {
        var substr_len = rand.nextInt(str.length());
        var substr_start = rand.nextInt(str.length() - substr_len + 1);
        return str.substring(substr_start, substr_start + substr_len);
    }

    public static BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(BigDecimal.valueOf(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

    private String getUserToken() {
        var test_user = new AuthRequest()
                .withUsername("test")
                .withPassword("test");
        return RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(test_user)
                .post("/auth/signin")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().header(HttpHeaders.AUTHORIZATION);
    }

    private String getRootToken() {
        var root_user = new AuthRequest()
                .withUsername("root")
                .withPassword("root");
        return RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(root_user)
                .post("/auth/signin")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().header(HttpHeaders.AUTHORIZATION);
    }

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    @BeforeAll
    public static void registerComparators() {
        var conf = new Configuration();
        conf.apply();
    }

    private NewArticleDto createRandomArticle() {
        var article = new NewArticleDto();
        article.setName(randomAlphabetic(10));
        return article;
    }

    private ArticleDto createArticleWithId(NewArticleDto article) {
        var user_token = getUserToken();
        return RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(article)
                .log().all()
                .post("/articles/new")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.CREATED.value())
                .extract().body().as(ArticleDto.class);
    }

    private String createArticleAsUri(ArticleDto article) {
        return "/articles/" + article.getId();
    }

    private BalanceDto createNewBalanceWithId() {
        var user_token = getUserToken();
        return RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .post("/balances/new")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.CREATED.value())
                .extract().body().as(BalanceDto.class);
    }

    private String createNewBalanceAsUri(BalanceDto balance) {
        return ("/balances/" + balance.getId());
    }

    private NewOperation createRandomOperation() {
        BigDecimal credit = generateRandomBigDecimalFromRange(
                BigDecimal.valueOf(0.01).setScale(2, RoundingMode.HALF_UP),
                BigDecimal.valueOf(10000.00).setScale(2, RoundingMode.HALF_UP)
        );
        BigDecimal debit = generateRandomBigDecimalFromRange(
                BigDecimal.valueOf(0.01).setScale(2, RoundingMode.HALF_UP),
                BigDecimal.valueOf(10000.00).setScale(2, RoundingMode.HALF_UP)
        );
        var op = new NewOperation();
        op.setCredit(credit);
        op.setDebit(debit);
        return op;
    }

    private OperationDto createOperationWithId(NewOperation operation, Integer article_id, Integer balance_id) {
        var user_token = getUserToken();
        operation.setArticleId(article_id);
        operation.setBalanceId(balance_id);
        return RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(operation)
                .post("/operations/register")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.CREATED.value())
                .extract().body().as(OperationDto.class);
    }

    private String createOperationAsUri(OperationDto operation) {
        return "/operations/" + operation.getId();
    }

    @Test
    public void whenGetAllArticles_thenOK() {
        RestAssured.given()
                .get("/articles")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void whenGetArticleById_thenOK() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var location = createArticleAsUri(article);
        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .log().headers()
                .get(location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(ArticleDto.class);

        assertThat(response.getId()).isEqualTo(article.getId());
        assertThat(response.getName()).isEqualTo(article.getName());
        assertThat(response.getCreated_at()).isCloseTo(article.getCreated_at(), within(100, ChronoUnit.MILLIS));
    }

    @Test
    public void whenGetArticleByTitle_thenOK() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var id_location = createArticleAsUri(article);
        var findByTitle = "/articles/find";
        RestAssured.given()
                .queryParam("title", article.getName())
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(findByTitle)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void getArticleById_sameAs_getArticleByTitle() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var id_location = createArticleAsUri(article);
        var findByTitle = "/articles/find";

        var resp_by_id = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(id_location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(ArticleDto.class);
        var resp_by_title = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("title", article.getName())
                .get(findByTitle)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(ArticleDto.class);

        assertThat(resp_by_id)
                .usingRecursiveComparison()
                .isEqualTo(resp_by_title);
    }

    @Test
    public void whenArticleDeleted_thenNotFound() {
        var user_token = getUserToken();
        var root_token = getRootToken();
        var article = createArticleWithId(createRandomArticle());
        var location = createArticleAsUri(article);
        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete(location)
                .then()
                .statusCode(HttpStatus.OK.value());
        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(location)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void whenUpdate_thenNewName() {
        var user_token = getUserToken();
        var orig_article = createArticleWithId(createRandomArticle());
        var location = createArticleAsUri(orig_article);

        var new_article = createRandomArticle();
        var saved_article = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(new_article)
                .put(location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(ArticleDto.class);

        assertThat(saved_article.getName())
                .isEqualTo(new_article.getName())
                .isNotEqualTo(orig_article.getName());
    }

    @Test
    public void whenDeleteNonExisting_thenNotFound() {
        var root_token = getRootToken();
        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete("/articles/1234567/delete")
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void whenPruneArticles_thenOk() {
        var user_token = getUserToken();
        var root_token = getRootToken();
        var blank_articles_len = 10;
        var articles = IntStream.range(0, blank_articles_len)
                .mapToObj(x -> createArticleWithId(createRandomArticle()))
                .toList();
        Comparator<LocalDateTime> truncateSeconds = (a, b) ->
                a.isAfter(b.truncatedTo(ChronoUnit.SECONDS)) ? 0 : 1;
        Comparator<LocalDateTime> closeEnough = (a, b) ->
                within(100, ChronoUnit.MILLIS).isBeyondOffset(a, b) ? 0 : 1;

        var all = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/articles")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", ArticleDto.class);
        assertThat(all)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withIgnoredFields("created_at")
                                .withComparatorForFields(closeEnough, "created_at").build())
                .containsAll(articles);

        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete("/articles/prune")
                .then()
                .statusCode(HttpStatus.OK.value());

        var left = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/articles")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", ArticleDto.class);
        assertThat(left)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withIgnoredFields("created_at")
                                .withComparatorForFields(closeEnough, "created_at").build())
                .doesNotContainAnyElementsOf(articles);
    }

    @Test
    public void whenGetAllBalances_thenOK() {
        RestAssured.given()
                .get("/balances")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void whenNewBalance_thenOk() {
        var balance = createNewBalanceWithId();
        assertThat(balance.getCredit())
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(BigDecimal.valueOf(0));
        assertThat(balance.getDebit())
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(BigDecimal.valueOf(0));
        assertThat(balance.getAmount())
                .usingComparator(BigDecimal::compareTo)
                .isEqualTo(BigDecimal.valueOf(0));
    }

    @Test
    public void whenBalanceGetById_thenOk() {
        var balance = createNewBalanceWithId();
        var location = createNewBalanceAsUri(balance);

        var get_balance = RestAssured.given()
                .get(location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(BalanceDto.class);

        assertThat(get_balance)
                .usingRecursiveComparison()
                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class)
                .isEqualTo(balance);

    }

    @Test
    public void whenFindCompareGE_sameAsAll() {
        var all = RestAssured.given()
                .get("/balances")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);

        var minimal_amount = all.stream().map(BalanceDto::getAmount).min(BigDecimal::compareTo)
                .orElse(BigDecimal.ZERO);

        var compared = RestAssured.given()
                .queryParam("amount", minimal_amount)
                .queryParam("compare", Comparison.GreaterOrEqual)
                .get("/balances/compared").then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);

        assertThat(compared)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .hasSameElementsAs(all);
    }

    @Test
    public void whenFindCompareLE_sameAsAll() {
        var all = RestAssured.given()
                .get("/balances")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);

        var maximal_amount = all.stream().map(BalanceDto::getAmount).max(BigDecimal::compareTo)
                .orElse(BigDecimal.ZERO);

        var compared = RestAssured.given()
                .queryParam("amount", maximal_amount)
                .queryParam("compare", Comparison.LessOrEqual)
                .get("/balances/compared").then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);

        assertThat(compared)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .hasSameElementsAs(all);
    }

    @Test
    public void whenBalanceDeleted_thenNotFound() {
        var user_token = getUserToken();
        var root_token = getRootToken();
        var location = createNewBalanceAsUri(createNewBalanceWithId());
        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete(location)
                .then()
                .statusCode(HttpStatus.OK.value());
        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(location)
                .then()
                .statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void whenPruneBalances_thenOk() {
        var user_token = getUserToken();
        var root_token = getRootToken();
        var blank_balances_len = 10;
        var balances = IntStream.range(0, blank_balances_len)
                .mapToObj(x -> createNewBalanceWithId())
                .toList();

        var all = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/balances")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);
        assertThat(all)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .containsAll(balances);

        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete("/balances/prune")
                .then()
                .statusCode(HttpStatus.OK.value());

        var left = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/balances")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);
        assertThat(left)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .doesNotContainAnyElementsOf(balances);
    }

    @Test
    public void whenGetAllOperations_thenOK() {
        RestAssured.given()
                .get("/operations")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value());
    }

    @Test
    public void whenFindAllOperations_thenOK() {
        var all_ops = RestAssured.given()
                .get("/operations")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);
        var find_ops = RestAssured.given()
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);
        assertThat(find_ops).usingRecursiveComparison().isEqualTo(all_ops);
    }

    @Test
    public void whenRegisterOperation_thenOk() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());
        var location = createOperationAsUri(operation);

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(OperationDto.class);

        assertThat(response)
                .usingRecursiveComparison()
                .isEqualTo(operation);
    }

    @Test
    public void whenRegisterThenRollback_thenSame() {
        var user_token = getUserToken();
        var root_token = getRootToken();
        var article = createArticleWithId(createRandomArticle());
        var balance_before_expect = createNewBalanceWithId();
        var balance_location = createNewBalanceAsUri(balance_before_expect);
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance_before_expect.getId());
        var location = createOperationAsUri(operation);

        var balance_after_expect = new TransactionDto(operation).addToTransaction(
                new BalanceDto(balance_before_expect.getId(), balance_before_expect.getDebit(),
                        balance_before_expect.getCredit(), balance_before_expect.getAmount()));
        balance_after_expect.setAmount(balance_after_expect.getDebit().subtract(balance_after_expect.getCredit()));
        var balance_after = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get(balance_location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(BalanceDto.class);
        assertThat(balance_after)
                .usingRecursiveComparison()
                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class)
                .isEqualTo(balance_after_expect);

        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, root_token)
                .delete(location + "/rollback")
                .then()
                .statusCode(HttpStatus.OK.value());

        var balance_rollback = RestAssured.given()
                .get(balance_location)
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().as(BalanceDto.class);
        assertThat(balance_rollback)
                .usingRecursiveComparison()
                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class)
                .isEqualTo(balance_before_expect);
    }

    @Test
    public void priceHistory_Ok() {
        var user_token = getUserToken();
        var operation_len = 6;
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var prices = IntStream.range(0, operation_len)
                .mapToObj(x -> createOperationWithId(createRandomOperation(), article.getId(), balance.getId()))
                .map(OperationDto::getAmount)
                .toList();

        var resp_prices = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/articles/" + article.getId() + "/history")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BigDecimal.class);

        assertThat(resp_prices)
                .usingRecursiveComparison()
                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class)
                .isEqualTo(prices);
    }

    @Test
    public void buyerHistory_Ok() {
        var user_token = getUserToken();
        var operation_len = 6;
        var article = createArticleWithId(createRandomArticle());
        var balances = IntStream.range(0, operation_len)
                .mapToObj(x -> createNewBalanceWithId()).toList();
        var operations = balances.stream()
                .map(balance -> createOperationWithId(createRandomOperation(), article.getId(), balance.getId()))
                .toList();
        balances = balances.stream().map(b ->
                RestAssured.given()
                        .get(createNewBalanceAsUri(b))
                        .then()
                        .contentType(ContentType.JSON)
                        .statusCode(HttpStatus.OK.value())
                        .extract().as(BalanceDto.class)).toList();

        var resp_buyers = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .get("/articles/" + article.getId() + "/buyers")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", BalanceDto.class);

        assertThat(resp_buyers)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .containsAll(balances);
    }

    @Test
    public void whenFindOperationByArticleId_thenFound() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("article_id", article.getId())
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);

        assertThat(response)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .contains(operation);
    }


    @Test
    public void whenFindOperationByArticleName_thenFound() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("article_name", article.getName())
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);

        assertThat(response)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .contains(operation);
    }

    @Test
    public void whenFindOperationByBalanceId_thenFound() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("balance_id", balance.getId())
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);

        assertThat(response)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .contains(operation);
    }

    @Test
    public void whenFindOperationByArticleIdAndBalanceId_thenFound() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("article_id", article.getId())
                .queryParam("balance_id", balance.getId())
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);

        assertThat(response)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .contains(operation);
    }

    @Test
    public void whenFindOperationByArticleNameAndBalanceId_thenFound() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        var response = RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("article_name", article.getName())
                .queryParam("balance_id", balance.getId())
                .get("/operations/find")
                .then()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().body().jsonPath().getList(".", OperationDto.class);

        assertThat(response)
                .usingRecursiveFieldByFieldElementComparator(
                        RecursiveComparisonConfiguration.builder()
                                .withComparatorForType(BigDecimal::compareTo, BigDecimal.class).build())
                .contains(operation);
    }


    @Test
    public void whenFindOperationByArticleIdAndArticleName_thenError() {
        var user_token = getUserToken();
        var article = createArticleWithId(createRandomArticle());
        var balance = createNewBalanceWithId();
        var operation = createOperationWithId(createRandomOperation(), article.getId(), balance.getId());

        RestAssured.given()
                .header(HttpHeaders.AUTHORIZATION, user_token)
                .queryParam("article_name", article.getName())
                .queryParam("article_id", article.getId())
                .get("/operations/find")
                .then()
                .log().body()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.BAD_REQUEST.value());
    }
}
