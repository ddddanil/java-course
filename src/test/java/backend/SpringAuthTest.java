package backend;

import backend.dto.AuthRequest;
import backend.dto.AuthResponse;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringAuthTest {
    static final Random rand = new Random();
    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        RestAssured.port = port;
    }

    private AuthRequest createRandomUser() {
        return new AuthRequest()
                .withUsername(randomAlphabetic(10))
                .withPassword(randomAlphabetic(10));
    }

    private AuthRequest registerUser(AuthRequest request) {
        RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(request)
                .post("/auth/register")
                .then()
                .statusCode(HttpStatus.CREATED.value());
        return request;
    }

    @Test
    public void authNoUser_thenError() {
        var req = new AuthRequest()
                .withUsername("notauser")
                .withPassword("notapassword");
        RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .post("/auth/signin")
                .then()
                .log().body()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void authIncorrectPassword_thenError() {
        var req = createRandomUser();
        req = registerUser(req).withPassword("notapassword");
        RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .post("/auth/signin")
                .then()
                .log().body()
                .statusCode(HttpStatus.UNAUTHORIZED.value());
    }

    @Test
    public void authCorrect_thenOk() {
        var req = registerUser(createRandomUser());
        var resp = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(req)
                .post("/auth/signin")
                .then()
                .log().headers()
                .log().body()
                .contentType(ContentType.JSON)
                .statusCode(HttpStatus.OK.value())
                .extract().as(AuthResponse.class);
        assertThat(resp.getUsername())
                .isEqualTo(req.getUsername());
    }
}
