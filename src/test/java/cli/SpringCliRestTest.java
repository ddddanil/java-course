package cli;

import backend.WebApplication;
import backend.dto.*;
import okhttp3.OkHttpClient;
import org.assertj.core.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.DefaultUriBuilderFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringCliRestTest {
    static final Random rand = new Random();
    private RestClient client;
    @LocalServerPort
    private int port;

    String randomSubstring(String str) {
        var substr_len = rand.nextInt(str.length());
        var substr_start = rand.nextInt(str.length() - substr_len + 1);
        return str.substring(substr_start, substr_start + substr_len);
    }

    public static BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(BigDecimal.valueOf(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

    @Before
    public void setUp() {
        var endpoint = new DefaultUriBuilderFactory("http://localhost:" + port);
        var authClient = new OkHttpClient();
        var authInterceptor = new AuthorizationInterceptor(endpoint, authClient);
        authInterceptor.auth = new AuthRequest("test", "test");
        var client = new OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .build();
        this.client = new RestClient(endpoint, client);
    }

    @BeforeAll
    public static void registerComparators() {
        var conf = new Configuration();
        conf.apply();
    }

    private NewArticleDto createRandomArticle() {
        var article = new NewArticleDto();
        article.setName(randomAlphabetic(10));
        return article;
    }

    @Test
    public void ListArticles() {
        List<ArticleDto> articles = new java.util.ArrayList<>();
        assertThatCode(() -> articles.addAll(client.ListArticles())).doesNotThrowAnyException();
        assertThat(articles)
                .hasSizeGreaterThan(0);
    }

    @Test
    public void CreateArticle() {
        var newArticle = createRandomArticle();
        AtomicReference<ArticleDto> article = new AtomicReference<>();
        assertThatCode(() -> article.set(client.CreateArticle(newArticle))).doesNotThrowAnyException();
        assertThat(article.get())
                .extracting("name")
                .isEqualTo(newArticle.getName());
    }
}
