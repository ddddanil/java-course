package lab1.test;

import lab1.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestStrategies {
    Hero h;
    Random rand = new Random();
    static final Movement[] strats = {new Walk(), new Fly(), new Crawl()};

    @BeforeEach
    void setUp() {
        h = new Hero(new Vector2D(0, 0));
        rand = new Random();
    }

    @Test
    void noStrategy() {
        assertThrows(LabException.class, () -> h.move(new Vector2D(1, 1)));
    }

    @RepeatedTest(10)
    void setStrategy() {
        Movement strat = strats[rand.nextInt(strats.length)];
        assertDoesNotThrow(() -> h.setStrategy(strat));
        Assertions.assertEquals(h.getStrategy(), strat);
    }

    @RepeatedTest(20)
    void checkMovement() {
        setStrategy();
        Vector2D old_loc = h.getPosition();
        Vector2D mv_vec = new Vector2D(rand.nextDouble(), rand.nextDouble());
        assertDoesNotThrow(() -> h.move(mv_vec));
        assertEquals(h.getPosition(), old_loc.add(mv_vec));
    }
}
