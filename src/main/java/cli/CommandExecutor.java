package cli;

import backend.dto.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class CommandExecutor {
    private final RestClient client;

    public CommandExecutor(RestClient client) {
        this.client = client;
    }

    public void GetArticle(String title, Boolean addPriceHistory, Boolean addBuyerHistory) {
        try {
            var article = client.FindArticleByTitle(title);
            System.out.printf("%4s %15s %20s\n", "ID", "Created at", "Title");
            System.out.printf("%4d %15s %20s\n", article.getId(), article.getCreated_at().toString(), article.getName());
            if (addPriceHistory) {
                var prices = client.GetArticlePriceHistory(article.getId());
                System.out.printf("\nPrice history\n%6s\n", "Amount");
                for (var p: prices) {
                    System.out.println(p);
                }
            }
            if (addBuyerHistory) {
                var balances = client.GetArticleBuyerHistory(article.getId());
                System.out.printf("\nBuyer history\n%4s %7s %7s %7s\n", "ID", "Debit", "Credit", "Amount");
                for (var b: balances) {
                    System.out.printf("%4d %5.2f %5.2f %5.2f\n", b.getId(), b.getDebit(), b.getCredit(), b.getAmount());
                }
            }
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void ListArticles() {
        try {
            var articles = client.ListArticles();
            System.out.printf("%4s %15s %20s\n", "ID", "Created at", "Title");
            for (var a: articles) {
                System.out.printf("%4d %15s %20s\n", a.getId(), a.getCreated_at().toString(), a.getName());
            }
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void ListBalances(Optional<Comparison> comparison, Optional<BigDecimal> amount) {
        try {
            List<BalanceDto> balances;
            if (comparison.isPresent() && amount.isPresent()) {
                balances = client.FilterBalancesWithAmount(comparison.get(), amount.get());
            } else {
                balances = client.ListBalances();
            }
            System.out.printf("%4s %7s %7s %7s\n", "ID", "Debit", "Credit", "Amount");
            for (var b: balances) {
                System.out.printf("%4d %5.2f %5.2f %5.2f\n", b.getId(), b.getDebit(), b.getCredit(), b.getAmount());
            }
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void ListOperations(Optional<String> article_name, Optional<Integer> balance_id) {
        try {
            List<OperationDto> operations;
            if (article_name.isPresent() || balance_id.isPresent()) {
                operations = client.FilterOperations(article_name, Optional.empty(), balance_id);
            } else {
                operations = client.ListOperations();
            }
            System.out.printf("%4s %4s %4s %7s %7s %7s\n", "ID", "Article ID", "Balance ID", "Debit", "Credit", "Amount");
            for (var o: operations) {
                System.out.printf("%4s %4d %4d %5.2f %5.2f %5.2f\n", o.getId(), o.getArticle_id(), o.getBalance_id(), o.getDebit(), o.getCredit(), o.getAmount());
            }
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void AddArticle(String title) {
        try {
            var new_article = new NewArticleDto()
                    .withName(title);
            var article = client.CreateArticle(new_article);
            System.out.printf("%4s %15s %20s\n", "ID", "Created at", "Title");
            System.out.printf("%4d %15s %20s\n", article.getId(), article.getCreated_at().toString(), article.getName());
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void AddBalance() {
        try {
            var balance = client.CreateBalance();
            System.out.printf("%4s %7s %7s %7s\n", "ID", "Debit", "Credit", "Amount");
            System.out.printf("%4d %5.2f %5.2f %5.2f\n", balance.getId(), balance.getDebit(), balance.getCredit(), balance.getAmount());
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void AddOpertaion(String articleName, Integer balance_id, BigDecimal debit, BigDecimal credit) {
        try {
            var article = client.FindArticleByTitle(articleName);
            var newOperation = new NewOperation()
                    .withArticleId(article.getId())
                    .withBalanceId(balance_id)
                    .withDebit(debit)
                    .withCredit(credit);
            var operation = client.CreateOperation(newOperation);
            System.out.printf("%4s %4s %4s %7s %7s %7s\n", "ID", "Article ID", "Balance ID", "Debit", "Credit", "Amount");
            System.out.printf("%4s %4d %4d %5.2f %5.2f %5.2f\n", operation.getId(), operation.getArticle_id(), operation.getBalance_id(), operation.getDebit(), operation.getCredit(), operation.getAmount());
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void UpdateArticle(String oldName, String newName) {
        try {
            var article = client.FindArticleByTitle(oldName);
            var newArticle = new NewArticleDto()
                    .withName(newName);
            article = client.UpdateArticleById(article.getId(), newArticle);
            System.out.printf("%4s %15s %20s\n", "ID", "Created at", "Title");
            System.out.printf("%4d %15s %20s\n", article.getId(), article.getCreated_at().toString(), article.getName());
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void DeleteArticle(String articleName) {
        try {
            var article = client.FindArticleByTitle(articleName);
            var res = client.DeleteArticleById(article.getId());
            System.out.println(res? "Deleted" : "Could not delete");
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void DeleteBalance(Integer balanceId) {
        try {
            var res = client.DeleteBalanceById(balanceId);
            System.out.println(res? "Deleted" : "Could not delete");
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void DeleteOperation(Integer operationId) {
        try {
            var res = client.DeleteOperationById(operationId);
            System.out.println(res? "Deleted" : "Could not delete");
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void PruneArticles() {
        try {
            client.PruneArticles();
            System.out.println("Done");
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }

    public void PruneBalances() {
        try {
            client.PruneBalances();
            System.out.println("Done");
        } catch (IOException e) {
            System.err.printf("Error: %s", e.getMessage());
        }
    }
}
