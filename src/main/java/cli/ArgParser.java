package cli;

import backend.dto.Comparison;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.annotation.Arg;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;

import java.math.BigDecimal;
import java.util.Optional;

public class ArgParser {
    private static class Options {
        @Arg(dest = "action")
        public String action;

        @Arg(dest = "object")
        public String object;
    }

    public static ArgumentParser argParser() {
        var parser = ArgumentParsers.newFor("balancer")
                .singleMetavar(true)
                .build()
                .defaultHelp(true)
                .version("v1.0.0")
                .description("Interact with the REST API");
        parser.addArgument("-u", "--user")
                .metavar("USER")
                .type(String.class)
                .help("Username for authentication");
        parser.addArgument("-p", "--password")
                .metavar("PASSWORD")
                .type(String.class)
                .help("Password for authentication");
        parser.addArgument("-H", "--host")
                .metavar("HOSTNAME")
                .type(String.class)
                .help("Hostname of the REST API server");
        var subparsers = parser.addSubparsers().dest("action");
        // Get verb
        var get_subparsers = subparsers.addParser("get")
                .addSubparsers().dest("object");
        var get_articles = get_subparsers.addParser("article");
        get_articles.addArgument("-a", "--article")
                .metavar("ARTICLE")
                .type(String.class)
                .help("Name of the article");
        var article_info = get_articles.addMutuallyExclusiveGroup("Additional info");
        article_info.addArgument("--price-history")
                .action(Arguments.storeTrue())
                .help("Display article price history");
        article_info.addArgument("--buyer-history")
                .action(Arguments.storeTrue())
                .help("Display article buyer history");
        // List verb
        var list_subparsers = subparsers.addParser("list")
                .addSubparsers().dest("object");
        var list_articles = list_subparsers.addParser("articles");
        var list_balances = list_subparsers.addParser("balances");
        var comparison_group = list_balances.addArgumentGroup("Filters");
        comparison_group.addArgument("-c", "--comparing")
                .type(Comparison.class)
                .nargs("?")
                .help("Compare balance amount");
        comparison_group.addArgument("-a", "--amount")
                .metavar("AMOUNT")
                .type(BigDecimal.class)
                .nargs("?")
                .setDefault(BigDecimal.ZERO)
                .help("Amount to compare to");
        var list_operations = list_subparsers.addParser("operations");
        list_operations.addArgument("-a", "--article")
                .metavar("ARTICLE")
                .type(String.class)
                .nargs("?")
                .help("Filter by article");
        list_operations.addArgument("-b", "--balance")
                .metavar("BALANCE_ID")
                .type(Integer.class)
                .nargs("?")
                .help("Filter by balance");
        // Add verb
        var add_subparsers = subparsers.addParser("add")
                .addSubparsers().dest("object");
        var add_articles = add_subparsers.addParser("article");
        add_articles.addArgument("-n", "--name")
                .metavar("NAME")
                .type(String.class)
                .help("Name of the new article");
        var add_balances = add_subparsers.addParser("balance");
        var add_operations = add_subparsers.addParser("operation");
        add_operations.addArgument("-a", "--article")
                .metavar("ARTICLE")
                .type(String.class)
                .help("Name of the bought article");
        add_operations.addArgument("-b", "--balance")
                .metavar("BALANCE_ID")
                .type(Integer.class)
                .help("ID of the buying balance");
        add_operations.addArgument("-d", "--debit")
                .metavar("DEBIT")
                .type(BigDecimal.class)
                .nargs("?")
                .setDefault(BigDecimal.ZERO)
                .help("Amount of debit");
        add_operations.addArgument("-c", "--credit")
                .metavar("CREDIT")
                .type(BigDecimal.class)
                .nargs("?")
                .setDefault(BigDecimal.ZERO)
                .help("Amount of credit");
        // Update verb
        var update_subparsers = subparsers.addParser("update")
                .addSubparsers().dest("object");
        var update_article = update_subparsers.addParser("article");
        update_article.addArgument("-n", "--name")
                .metavar("NAME")
                .type(String.class)
                .help("Name of the old article");
        update_article.addArgument("-N", "--new-name")
                .metavar("NEW_NAME")
                .type(String.class)
                .help("New name of the article");
        // Delete verb
        var delete_subparsers = subparsers.addParser("delete")
                .addSubparsers().dest("object");
        var delete_article = delete_subparsers.addParser("article");
        delete_article.addArgument("-a", "--article")
                .metavar("ARTICLE")
                .type(String.class)
                .help("Name of the deleted article");
        var delete_balance = delete_subparsers.addParser("balance");
        delete_balance.addArgument("-b", "--balance")
                .metavar("BALANCE_ID")
                .type(Integer.class)
                .help("ID of the deleted balance");
        var delete_operation = delete_subparsers.addParser("operation");
        delete_operation.addArgument("-o", "--operation")
                .metavar("OPERATION_ID")
                .type(Integer.class)
                .help("ID of the deleted operation");
        // Prune verb
        var prune_subparsers = subparsers.addParser("prune")
                .addSubparsers().dest("object");
        var prune_article = prune_subparsers.addParser("articles");
        var prune_balance = prune_subparsers.addParser("balances");
        return parser;
    }

    public static Boolean Dispatch(Namespace values, CommandExecutor cli) {
        return switch (values.getString("action")) {
            case "get" -> switch (values.getString("object")) {
                case "article" -> {
                    var title = values.getString("article");
                    var price_history = values.getBoolean("price_history");
                    var buyer_history = values.getBoolean("buyer_history");
                    cli.GetArticle(title, price_history, buyer_history);
                    yield true;
                }
                default -> false;
            };
            case "list" -> switch (values.getString("object")) {
                case "articles" -> {
                    cli.ListArticles();
                    yield true;
                }
                case "balances" -> {
                    Optional<Comparison> comparison = Optional.ofNullable(values.get("comparing"));
                    Optional<BigDecimal> amount = Optional.ofNullable(values.get("amount"));
                    cli.ListBalances(comparison, amount);
                    yield true;
                }
                case "operations" -> {
                    Optional<String> article_name = Optional.ofNullable(values.getString("article"));
                    Optional<Integer> balance_id = Optional.ofNullable(values.getInt("balance"));
                    cli.ListOperations(article_name, balance_id);
                    yield true;
                }
                default -> false;
            };
            case "add" -> switch (values.getString("object")) {
                case "article" -> {
                    var name = values.getString("name");
                    cli.AddArticle(name);
                    yield true;
                }
                case "balance" -> {
                    cli.AddBalance();
                    yield true;
                }
                case "operation" -> {
                    String article_name = values.getString("article");
                    Integer balance_id = values.getInt("balance");
                    BigDecimal debit = values.get("debit");
                    BigDecimal credit = values.get("credit");
                    cli.AddOpertaion(article_name, balance_id, debit, credit);
                    yield true;
                }
                default -> false;
            };
            case "update" -> switch (values.getString("object")) {
                case "article" -> {
                    var name = values.getString("name");
                    var newName = values.getString("new_name");
                    cli.UpdateArticle(name, newName);
                    yield true;
                }
                default -> false;
            };
            case "delete" -> switch (values.getString("object")) {
                case "article" -> {
                    var name = values.getString("article");
                    cli.DeleteArticle(name);
                    yield true;
                }
                case "balance" -> {
                    Integer balance_id = values.getInt("balance");
                    cli.DeleteBalance(balance_id);
                    yield true;
                }
                case "operation" -> {
                    Integer operation_id = values.getInt("operation");
                    cli.DeleteOperation(operation_id);
                    yield true;
                }
                default -> false;
            };
            case "prune" -> switch (values.getString("object")) {
                case "articles" -> {
                    cli.PruneArticles();
                    yield true;
                }
                case "balances" -> {
                    cli.PruneBalances();
                    yield true;
                }
                default -> false;
            };
            default -> false;
        };
    }
}
