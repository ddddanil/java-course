package cli;

import backend.dto.AuthRequest;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import okhttp3.OkHttpClient;
import org.springframework.web.util.DefaultUriBuilderFactory;

public class CliApp {
    private static CommandExecutor initExecutor(String endpoint, AuthRequest auth) {
        var endpointFactory = new DefaultUriBuilderFactory(endpoint);
        var authClient = new OkHttpClient();
        var authInterceptor = new AuthorizationInterceptor(endpointFactory, authClient);
        authInterceptor.auth = auth;
        var restClient = new OkHttpClient.Builder()
                .addInterceptor(authInterceptor)
                .build();
        var client = new RestClient(endpointFactory, restClient);
        return new CommandExecutor(client);
    }

    public static void main(String[] argv) {
        var argparser = ArgParser.argParser();
        try {
            var res = argparser.parseArgs(argv);
            System.err.println(res);
            var hostname = res.getString("host");
            var username = res.getString("user");
            var password = res.getString("password");
            var auth = new AuthRequest()
                    .withUsername(username)
                    .withPassword(password);
            var executor = initExecutor(hostname, auth);
            ArgParser.Dispatch(res, executor);
        } catch (ArgumentParserException e) {
            argparser.handleError(e);
        }
    }
}
