package cli;

import backend.dto.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import okhttp3.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriBuilderFactory;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class RestClient {
    private final UriBuilderFactory endpoint;
    private final OkHttpClient client;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");

    public RestClient(UriBuilderFactory endpoint, OkHttpClient client) {
        this.endpoint = endpoint;
        this.client = client;
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    //region Articles

    public List<ArticleDto> ListArticles() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles")
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<ArticleDto>>(){});
        }
    }

    public ArticleDto CreateArticle(NewArticleDto newArticle) throws IOException {
        var json_value = objectMapper.writeValueAsString(newArticle);
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", "new")
                        .build().toString())
                .post(RequestBody.create(json_value, MEDIA_TYPE_JSON))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, ArticleDto.class);
        }
    }

    public ArticleDto FindArticleByTitle(String title) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", "find")
                        .queryParam("title", title)
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with title " + title + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, ArticleDto.class);
        }
    }

    public ArticleDto FindArticleById(Integer article_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", article_id.toString())
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with id " + article_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, ArticleDto.class);
        }
    }

    public ArticleDto UpdateArticleById(Integer article_id, NewArticleDto newArticle) throws IOException {
        var json_value = objectMapper.writeValueAsString(newArticle);
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", article_id.toString())
                        .build().toString())
                .put(RequestBody.create(json_value, MEDIA_TYPE_JSON))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with id " + article_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, ArticleDto.class);
        }
    }

    public Boolean DeleteArticleById(Integer article_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", article_id.toString())
                        .build().toString())
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with id " + article_id + " was not found");
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public void PruneArticles() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", "prune")
                        .build().toString())
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    public List<BigDecimal> GetArticlePriceHistory(Integer article_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", article_id.toString(), "history")
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with id " + article_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<BigDecimal>>() {});
        }
    }

    public List<BalanceDto> GetArticleBuyerHistory(Integer article_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("articles", article_id.toString(), "buyers")
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Article with id " + article_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<BalanceDto>>() {});
        }
    }

    //endregion
    //region Balances

    public List<BalanceDto> ListBalances() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances")
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<BalanceDto>>(){});
        }
    }

    public List<BalanceDto> FilterBalancesWithAmount(Comparison comparison, BigDecimal amount) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", "compared")
                        .queryParam("compare", comparison)
                        .queryParam("amount", amount)
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<BalanceDto>>(){});
        }
    }

    public BalanceDto CreateBalance() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", "new")
                        .build().toString())
                .post(RequestBody.create("", MEDIA_TYPE_JSON))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, BalanceDto.class);
        }
    }

    public BalanceDto FindBalanceById(Integer balance_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", balance_id.toString())
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Balance with id " + balance_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, BalanceDto.class);
        }
    }

    public Boolean DeleteBalanceById(Integer balance_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", balance_id.toString())
                        .build().toString())
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Balance with id " + balance_id + " was not found");
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    public void PruneBalances() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", "prune")
                        .build().toString())
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code " + response);
            }
        }
    }

    //endregion
    //region Operations

    public List<OperationDto> ListOperations() throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("operations")
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<OperationDto>>(){});
        }
    }

    public List<OperationDto> FilterOperations(Optional<String> article_name, Optional<Integer> article_id, Optional<Integer> balance_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("balances", "compared")
                        .queryParam("article_name", article_name)
                        .queryParam("article_id", article_id)
                        .queryParam("balance_id", balance_id)
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, new TypeReference<List<OperationDto>>(){});
        }
    }

    public OperationDto CreateOperation(NewOperation newOperation) throws IOException {
        var json_value = objectMapper.writeValueAsString(newOperation);
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("operations", "register")
                        .build().toString())
                .post(RequestBody.create(json_value, MEDIA_TYPE_JSON))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, OperationDto.class);
        }
    }

    public OperationDto FindOperationById(Integer operation_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("operations", operation_id.toString())
                        .build().toString())
                .get()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Operation with id " + operation_id + " was not found");
                } else {
                    throw new IOException("Unexpected code " + response);
                }
            }
            if (response.body() == null) throw new IOException("Response body is null");

            var resp_json = response.body().byteStream();
            return objectMapper.readValue(resp_json, OperationDto.class);
        }
    }

    public Boolean DeleteOperationById(Integer operation_id) throws IOException {
        var request = new Request.Builder()
                .url(endpoint.builder()
                        .pathSegment("operations", operation_id.toString(), "rollback")
                        .build().toString())
                .delete()
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                if (response.code() == HttpStatus.NOT_FOUND.value()) {
                    throw new EntityNotFoundException("Operation with id " + operation_id + " was not found");
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    //endregion
}
