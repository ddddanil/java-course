package cli;

import backend.dto.ArticleDto;
import backend.dto.AuthRequest;
import com.diffplug.common.base.Errors;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UriBuilderFactory;

import javax.security.sasl.AuthenticationException;
import java.io.IOException;
import java.util.Optional;

public class AuthorizationInterceptor implements Interceptor {
    private Optional<String> token;
    public AuthRequest auth;
    private final UriBuilderFactory endpoint;
    private final OkHttpClient client;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");

    public AuthorizationInterceptor(UriBuilderFactory endpoint, OkHttpClient client) {
        this.token = Optional.empty();
        this.endpoint = endpoint;
        this.client = client;
    }

    private String AcquireToken() throws IOException {
        var json_value = objectMapper.writeValueAsString(auth);
        var request = new Request.Builder()
                .url(endpoint.builder().pathSegment("auth", "signin").build().toString())
                .post(RequestBody.create(json_value, MEDIA_TYPE_JSON))
                .build();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            if (response.body() == null) throw new IOException("Response body is null");

            return response.header(HttpHeaders.AUTHORIZATION);
        }
    }

    private String GetCachedToken() {
        this.token = Optional.ofNullable(token.orElseGet(Errors.rethrow().wrap(this::AcquireToken)));
        if (token.isEmpty()) throw new AuthorizationException("Authorization error");
        return this.token.get();
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Interceptor.Chain chain) throws IOException {
        var request = chain.request();
        var with_auth = request.newBuilder()
                .header(HttpHeaders.AUTHORIZATION, GetCachedToken())
                .build();
        var response = chain.proceed(with_auth);
        if (response.code() == HttpStatus.UNAUTHORIZED.value()) throw new AuthenticationException("Not authorized");
        if (response.code() == HttpStatus.FORBIDDEN.value()) throw new AuthenticationException("Forbidden");
        return response;
    }
}
