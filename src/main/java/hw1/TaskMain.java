package hw1;

import java.util.List;

import static hw1.md.Element.*;

public class TaskMain {
    public static void main(String[] argv) {
        var markdown = Paragraph(List.of(
                Strong(List.of(
                        Text("1"),
                        Strikeout(List.of(
                                Text("2"),
                                Emphasis(List.of(
                                        Text("3"),
                                        Text("4")
                                )),
                                Text("5")
                        )),
                        Text("6")
                ))
        ));
        var builder = new StringBuilder();
        markdown.toMarkdown(builder);
        System.out.println(builder);
    }
}
