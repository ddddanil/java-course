package hw1.md;

import java.util.Collections;
import java.util.List;

public class Element implements ToMarkdown {
    private final Iterable<ToMarkdown> contents;
    private final String surround_left;
    private final String surround_right;

    public void toMarkdown(StringBuilder builder) {
        builder.append(this.surround_left);
        this.contents.forEach((c) -> c.toMarkdown(builder));
        builder.append(this.surround_right);
    }

    private Element(Iterable<ToMarkdown> contents, String surround_left, String surround_right) {
        this.contents = contents;
        this.surround_left = surround_left;
        this.surround_right = surround_right;
    }

    // Convenience constructors

    private static Element Surround(Iterable<ToMarkdown> contents, String surround) {
        return new Element(contents, surround, surround);
    }

    public static Element Paragraph(Iterable<ToMarkdown> contents) {
        return Surround(contents, "");
    }

    public static Element Text(String contents) {
        return Surround(Collections.singleton((builder) -> builder.append(contents)), "");
    }

    public static Element Strong(Iterable<ToMarkdown> contents) {
        return Surround(contents, "__");
    }

    public static Element Strong(ToMarkdown element) {
        return Surround(Collections.singleton(element), "__");
    }

    public static Element Strong(String content) {
        return Strong(Text(content));
    }

    public static Element Emphasis(Iterable<ToMarkdown> contents) {
        return Surround(contents, "*");
    }

    public static Element Emphasis(ToMarkdown element) {
        return Surround(Collections.singleton(element), "*");
    }

    public static Element Emphasis(String content) {
        return Emphasis(Text(content));
    }

    public static Element Strikeout(Iterable<ToMarkdown> contents) {
        return Surround(contents, "~");
    }

    public static Element Strikeout(ToMarkdown element) {
        return Surround(Collections.singleton(element), "~");
    }

    public static Element Strikeout(String content) {
        return Strikeout(Text(content));
    }

    public static Element Code(String contents) {
        return Surround(Collections.singleton(Text(contents)), "`");
    }

    public static Element CodeBlock(String contents) {
        return new Element(Collections.singleton(Text(contents)), "```\n", "\n```");
    }

    public static Element CodeBlock(String contents, String language) {
        return new Element(Collections.singleton(Text(contents)), "```" + language + "\n", "\n```");
    }

    public static Element Heading(Iterable<ToMarkdown> contents, int level) {
        if(level < 1 || level > 6)
            throw new IllegalArgumentException("Illegal Markdown heading level");
        return new Element(contents, "#".repeat(level) + " ", "\n");
    }

    public static Element Heading1(Iterable<ToMarkdown> contents) {
        return Heading(contents, 1);
    }

    public static Element Heading1(ToMarkdown element) {
        return Heading(Collections.singleton(element), 1);
    }

    public static Element Heading1(String content) {
        return Heading(Collections.singleton(Text(content)), 1);
    }

    public static Element Heading2(Iterable<ToMarkdown> contents) {
        return Heading(contents, 2);
    }

    public static Element Heading2(ToMarkdown element) {
        return Heading(Collections.singleton(element), 2);
    }

    public static Element Heading2(String content) {
        return Heading(Collections.singleton(Text(content)), 2);
    }

    public static Element Heading3(Iterable<ToMarkdown> contents) {
        return Heading(contents, 3);
    }

    public static Element Heading3(ToMarkdown element) {
        return Heading(Collections.singleton(element), 3);
    }

    public static Element Heading3(String content) {
        return Heading(Collections.singleton(Text(content)), 3);
    }

    public static Element Heading4(Iterable<ToMarkdown> contents) {
        return Heading(contents, 4);
    }

    public static Element Heading4(ToMarkdown element) {
        return Heading(Collections.singleton(element), 4);
    }

    public static Element Heading4(String content) {
        return Heading(Collections.singleton(Text(content)), 4);
    }

    public static Element Heading5(Iterable<ToMarkdown> contents) {
        return Heading(contents, 5);
    }

    public static Element Heading5(ToMarkdown element) {
        return Heading(Collections.singleton(element), 5);
    }

    public static Element Heading5(String content) {
        return Heading(Collections.singleton(Text(content)), 5);
    }

    public static Element Heading6(Iterable<ToMarkdown> contents) {
        return Heading(contents, 6);
    }

    public static Element Heading6(ToMarkdown element) {
        return Heading(Collections.singleton(element), 6);
    }

    public static Element Heading6(String content) {
        return Heading(Collections.singleton(Text(content)), 6);
    }
}
