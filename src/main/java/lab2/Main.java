package lab2;

import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static class Paths {
        static final Path input_file = FileSystems.getDefault().getPath("task", "input.txt");
        static final Path output_file = FileSystems.getDefault().getPath("task", "output.txt");
    }

    static void doLabTask(Matrix matrix, PrintWriter out) {
        for (int i = 0; i < 3; ++i) {
            out.printf("%s%n", matrix);
            matrix = matrix.rotate90CW().labOperation();
        }
    }

    static int readSize(Scanner in) {
        try {
            int N = in.nextInt();
            if (N > 1_000_000) {
                throw new LabException("Value of N too large");
            }
            if (N <= 0) {
                throw new LabException("Value of N must be greater than 0");
            }
            return N;
        } catch (NoSuchElementException e) {
            throw new LabException("No size N", e);
        }
    }

    public static void main(String[] argv) {
        try (var input = Files.newBufferedReader(Paths.input_file);
                var output = Files.newOutputStream(Paths.output_file);
                var in = new Scanner(input);
                var out = new PrintWriter(output)) {
            var rand = new Random();
            var N = readSize(in);
            var matrix = Matrix.newRandom(N, rand);
            doLabTask(matrix, out);
        } catch (Exception e) {
            System.err.printf("ERROR: %s - %s", e, e.getMessage());
            e.printStackTrace(System.err);
        }
    }
}
