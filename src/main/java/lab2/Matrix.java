package lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Random;
import java.util.Scanner;
import java.util.function.ToDoubleBiFunction;

public class Matrix {
    private final int N;
    private final double[] matrix;

    public Matrix(int N, ToDoubleBiFunction<Integer, Integer> func) {
        this.N = N;
        this.matrix = new double[N * N];
        for(int i = 0; i < N; ++i) {
            for(int j = 0; j < N; ++j) {
                this.matrix[index(i, j)] = func.applyAsDouble(i, j);
            }
        }
    }

    public static Matrix newRandom(int N, Random rand) {
        return new Matrix(N, (i, j) -> 2 * N * rand.nextDouble() - N);
    }

    public int size() {
        return N;
    }

    private int index(int row, int col) {
        if (row >= N) {
            throw new LabException("Row out of bounds", new ArrayIndexOutOfBoundsException(row));
        }
        if (col >= N) {
            throw new LabException("Column out of bounds", new ArrayIndexOutOfBoundsException(col));
        }
        return row * N + col;
    }

    public double getMatrix(int row, int col) {
        return matrix[index(row, col)];
    }

    public void setMatrix(int row, int col, double val) {
        matrix[index(row, col)] = val;
    }

    public Matrix rotate90CW() {
        return new Matrix(N, (i, j) -> getMatrix(N - j - 1, i));
    }

    public Matrix rotate180CW() {
        return new Matrix(N, (i, j) -> getMatrix(N - i - 1, N - j - 1));
    }

    public Matrix rotate270CW() {
        return new Matrix(N, (i, j) -> getMatrix(j, N - i - 1));
    }

    public Matrix labOperation() {
        return new Matrix(N, (i, j) -> {
                if(N == 1) {
                    return getMatrix(i, j);
                }
                double sum = 0;
                int last = N - 1;
                if (i != 0) {
                    if(j != 0) {
                        sum += getMatrix(i - 1, j - 1);
                    }
                    if(j != last) {
                        sum += getMatrix(i - 1, j + 1);
                    }
                    sum += getMatrix(i - 1, j);
                }
                if (i != last) {
                    if (j != 0) {
                        sum += getMatrix(i + 1, j - 1);
                    }
                    if (j != last) {
                        sum += getMatrix(i + 1, j + 1);
                    }
                    sum += getMatrix(i + 1, j);
                }
                if (j != 0) {
                    sum += getMatrix(i, j - 1);
                }
                if (j != last) {
                    sum += getMatrix(i, j + 1);
                }
                if (sum == 0.0) {
                    throw new LabException("Division by zero");
                }
                return getMatrix(i, j) / sum;
        });
    }

    @Override
    public String toString() {
        var buf = new StringBuilder();
        for(int i = 0; i < N; ++i) {
            for(int j = 0; j < N; ++j) {
                buf.append(getMatrix(i, j));
                if (j != N - 1) {
                    buf.append(' ');
                }
            }
            buf.append('\n');
        }
        return buf.toString();
    }
}
