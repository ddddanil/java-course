package lab6;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ThreadPool implements Runnable {
    private static class Child {
        public Thread thread;
        public RandomThread state;

        public Child(RandomThread s) {
            this.thread = new Thread(s);
            this.state = s;
        }
    }

    public final Object sync = new Object();
    private final Collection<Child> children = new ArrayList<>();

    public void addChild(RandomThread task) {
        synchronized (children) {
            var newChild = new Child(task);
            children.add(newChild);
            newChild.thread.start();
            System.out.printf("Thread started \"%s\" (%d)\n", newChild.thread.getName(), newChild.thread.getId());
        }
    }

    public void run() {
        while(true) {
            try {
                synchronized (sync) {
                    sync.wait();
                }
                synchronized (children) {
                    for (var c : children) {
                        var state = c.state.getState();
                        if (state == ThreadState.UNKNOWN || state == ThreadState.STOPPED) {
                            c.thread.interrupt();
                            c.thread.join();
                            var oldthread = c.thread;
                            c.thread = new Thread(c.state);
                            c.thread.start();
                            System.out.printf("Thread restarted \"%s\"(%d) -> \"%s\"(%d)\n",
                                    oldthread.getName(), oldthread.getId(), c.thread.getName(), c.thread.getId());
                            break;
                        } else if (state == ThreadState.FATAL_ERROR) {
                            c.thread.interrupt();
                            c.thread.join();
                            children.remove(c);
                            System.out.printf("Thread stopped \"%s\"(%d)\n", c.thread.getName(), c.thread.getId());
                            break;
                        }
                    }
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
