package lab6;

public class Main {
    public static void main(String[] argv) {
        var supervisor = new ThreadPool();
        var supervisor_thread = new Thread(supervisor);
        supervisor_thread.start();
        try {
            supervisor.addChild(new RandomThread(supervisor.sync));
            supervisor.addChild(new RandomThread(supervisor.sync));
            supervisor.addChild(new RandomThread(supervisor.sync));
            Thread.sleep(600);
            supervisor.addChild(new RandomThread(supervisor.sync));
            Thread.sleep(400);
            supervisor.addChild(new RandomThread(supervisor.sync));
            supervisor.addChild(new RandomThread(supervisor.sync));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
