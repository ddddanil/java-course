package lab6;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public enum ThreadState {
    UNKNOWN, STOPPED, RUNNING, FATAL_ERROR;

    private static final List<ThreadState> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static ThreadState Random()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
