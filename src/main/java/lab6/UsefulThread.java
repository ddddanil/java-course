package lab6;


public class UsefulThread implements Runnable {
    private ThreadState state = ThreadState.UNKNOWN;
    private final Object state_mutex = new Object();
    private final Thread parent;
    private final Runnable task;

    public UsefulThread(Runnable task, Thread parent) {
        this.task = task;
        this.parent = parent;
    }

    public void run() {
        try {
            synchronized (state_mutex) {
                this.state = ThreadState.RUNNING;
            }
            task.run();
            synchronized (state_mutex) {
                this.state = ThreadState.STOPPED;
            }
            parent.notify();
        }
        catch (RuntimeException e) {
            synchronized (state_mutex) {
                this.state = ThreadState.FATAL_ERROR;
            }
            parent.notify();
        }
    }

    public ThreadState getState() {
        synchronized (state_mutex) {
            return this.state;
        }
    }
}
