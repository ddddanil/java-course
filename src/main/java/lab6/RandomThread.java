package lab6;

import java.util.Random;

public class RandomThread implements Runnable {
    private ThreadState state = ThreadState.UNKNOWN;
    private static final Random random = new Random();
    private final Object sync;

    public RandomThread(Object sync) {
        this.sync = sync;
    }

    public void run() {
        while (true) {
            synchronized (state) {
                this.state = ThreadState.Random();
            }
            synchronized (sync) {
                sync.notify();
            }
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public ThreadState getState() {
        synchronized (state) {
            return this.state;
        }
    }
}
