package lab4;

public class TestBed {
    private int secret;
    public int something;

    public TestBed(int a, int b) {
        secret = a;
        something = b;
    }

    @RunTimes(2)
    private void method1() {
        System.out.println("TestBed: method1");
    }

    @RunTimes(5)
    private void method2secret() {
        System.out.printf("TestBed: method2 secret = %d%n", secret);
    }

    @RunTimes(4)
    public void method3something() {
        System.out.printf("TestBed: method3 something = %d%n", something);
    }
}
