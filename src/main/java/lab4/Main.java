package lab4;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void runTimes(Object obj) {
        if(obj == null) {
            throw new LabException(new NullPointerException());
        }
        Class<?> cls = obj.getClass();
        for(var method: cls.getDeclaredMethods()) {
            if(method.isAnnotationPresent(RunTimes.class)) {
                var reps = method.getAnnotation(RunTimes.class).value();
                method.setAccessible(true);
                for(int i = 0; i < reps; ++i) {
                    try {
                        method.invoke(obj);
                    }
                    catch (IllegalAccessException | InvocationTargetException e) {
                        throw new LabException(e);
                    }
                }
            }
        }
    }

    public static void main(String[] argv) {
        TestBed test = new TestBed(3, 6);
        runTimes(test);
    }
}
