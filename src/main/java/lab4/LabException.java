package lab4;

public class LabException extends RuntimeException {
    public LabException(String message) {
        super(message);
    }

    public LabException(String message, Throwable cause) {
        super(message, cause);
    }

    public LabException(Throwable cause) {
        super(cause);
    }
}
