package lab5;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMethods {

    static Double getAverage(Stream<Double> st) {
        return st.mapToDouble((d) -> d).average().orElseThrow();
    }

    static Stream<String> changeReg(Stream<String> s) {
        return s.map(String::toUpperCase)
                .map("__new__"::concat);
    }

    static Stream<Integer> uniqueSquared(Stream<Integer> s) {
        return s.sorted()
                .distinct()
                .map((i) -> i * i);
    }

    static Stream<String> filterStartChar(Stream<String> s, char start) {
        return s.sorted()
                .filter((st) -> st.startsWith(Character.toString(start)));
    }

    static <T> T lastElem(Stream<T> s) {
        return s.reduce((first, second) -> second)
                .orElseThrow();
    }

    static Integer sumEven(Stream<Integer> s) {
        return s.filter((i) -> i % 2 == 0)
                .reduce(0, Integer::sum);
    }

    static Map<Character, String> createMap(Stream<String> s) {
        return s.collect(Collectors.toMap(
                (st) -> st.toCharArray()[0],
                (st) -> st.substring(1)
        ));
    }
}
