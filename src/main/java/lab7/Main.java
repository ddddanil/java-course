package lab7;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] argv) {
        int N = 7;
        var rand = new Random();
        var queue = new LinkedBlockingQueue<Message>(1);
        var threads = new ArrayList<Thread>();

        for(int i = 0; i < N; ++i) {
            threads.add(new MessageSender(queue, rand.nextInt(1000)).runThread());
            threads.add(new MessageReader(queue).runThread());
        }

        try {
            Thread.sleep(30 * 1000);
            for (Thread thread : threads) {
                thread.interrupt();
            }
            for (Thread thread : threads) {
                thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
