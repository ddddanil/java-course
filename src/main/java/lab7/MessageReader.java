package lab7;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class MessageReader implements Runnable {
    private static final Object readerIdLock = new Object();
    private static Long readerIdGen = 0L;
    private final long readerId;
    private final BlockingQueue<Message> source;

    public MessageReader(BlockingQueue<Message> source) {
        this.source = source;
        synchronized (readerIdLock) {
            this.readerId = readerIdGen++;
        }
    }

    private void getMessage() throws InterruptedException {
        Message m = source.take();
        long threadId = Thread.currentThread().getId();
        String threadName = Thread.currentThread().getName();
        synchronized (System.out) {
            System.out.printf("[Thread %3d %s] Got Message(%d)\n", threadId, threadName, m.payload());
        }
    }

    @Override
    public void run() {
        var rand = new Random();
        while(true) {
            try {
                int wait_millis = rand.nextInt(800);
                Thread.sleep(wait_millis);
                this.getMessage();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public Thread runThread() {
        var thread = new Thread(this);
        thread.setName(String.format("Reader #%d", this.readerId));
        thread.start();
        return thread;
    }
}
