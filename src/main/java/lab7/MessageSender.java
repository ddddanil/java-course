package lab7;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class MessageSender implements Runnable {
    private static final Object senderIdLock = new Object();
    private static Long senderIdGen = 0L;
    private final long senderId;
    private final BlockingQueue<Message> destination;
    private final int resource;

    public MessageSender(BlockingQueue<Message> destination, int resource) {
        this.destination = destination;
        this.resource = resource;
        synchronized (senderIdLock) {
            this.senderId = senderIdGen++;
        }
    }

    private void sendMessage() throws InterruptedException {
        var m = new Message(resource);
        long threadId = Thread.currentThread().getId();
        String threadName = Thread.currentThread().getName();
        synchronized (System.out) {
            System.out.printf("[Thread %3d %s] Sending Message(%d)\n", threadId, threadName, m.payload());
        }
        this.destination.put(m);
    }

    @Override
    public void run() {
        var rand = new Random();
        while(true) {
            try {
                int wait_millis = rand.nextInt(800);
                Thread.sleep(wait_millis);
                this.sendMessage();
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    public Thread runThread() {
        var thread = new Thread(this);
        thread.setName(String.format("Sender #%d", this.senderId));
        thread.start();
        return thread;
    }
}
