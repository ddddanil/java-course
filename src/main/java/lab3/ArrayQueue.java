package lab3;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

public class ArrayQueue<E> implements Queue<E> {
    private enum LastOp {
        Add, Remove
    }

    private final Vector<E> elements;
    private final int capacity;
    private int begin, end;
    private LastOp lastOp;

    public ArrayQueue(int len) {
        this.capacity = len;
        this.elements = new Vector<>(capacity);
        elements.setSize(capacity);
        this.begin = 0;
        this.end = 0;
        lastOp = LastOp.Remove;
    }

    public ArrayQueue<E> produce(Class<? extends E> cls) {
        return this.stream()
                .filter(cls::isInstance)
                .collect(toCollection(() -> new ArrayQueue<>(this.size())));
    }

    public ArrayQueue<E> consume(Class<? extends E> cls) {
        return this.stream()
                .filter(e -> e.getClass().isAssignableFrom(cls))
                .collect(toCollection(() -> new ArrayQueue<>(this.size())));
    }

    @Override
    public boolean isEmpty() {
        return (begin == end) && (lastOp.equals(LastOp.Remove));
    }

    public boolean isFull() {
        return (begin == end) && (lastOp.equals(LastOp.Add));
    }

    @Override
    public int size() {
        int s = (end - begin) % capacity;
        s = (s < 0) ? s + capacity : s; // Rem -> mod
        if (s == 0 && lastOp.equals(LastOp.Add)) {
            s = capacity;
        }
        return s;
    }

    @Override
    public boolean addAll(Collection<? extends E> coll) {
        if (capacity - size() < coll.size())
            throw new IllegalStateException("Not enough space in ArrayQueue");
        coll.iterator().forEachRemaining(this::add);
        return true;
    }

    @Override
    public boolean contains(Object o) {
        return this.viewList().contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> coll) {
        return this.viewList().containsAll(coll);
    }

    @Override
    public boolean add(E e) {
        if (isFull())
            throw new IllegalStateException("ArrayQueue is full");
        elements.set(end, e);
        end = (end + 1) % capacity;
        lastOp = LastOp.Add;
        return true;
    }

    @Override
    public E remove() {
        if (isEmpty())
            throw new NoSuchElementException("ArrayQueue is empty");
        E ret = elements.get(begin);
        elements.set(begin, null);
        begin = (begin + 1) % capacity;
        lastOp = LastOp.Remove;
        return ret;
    }

    @Override
    public E element() {
        if (isEmpty())
            throw new NoSuchElementException("ArrayQueue is empty");
        return elements.get(begin);
    }

    @Override
    public boolean offer(E e) {
        if (isFull())
            return false;
        elements.set(end, e);
        end = (end + 1) % capacity;
        lastOp = LastOp.Add;
        return true;
    }

    @Override
    public E poll() {
        if (isEmpty())
            return null;
        E ret = elements.get(begin);
        elements.set(begin, null);
        begin = (begin + 1) % capacity;
        lastOp = LastOp.Remove;
        return ret;
    }

    @Override
    public E peek() {
        if (isEmpty())
            return null;
        return elements.get(begin);
    }

    @Override
    public void clear() {
        elements.iterator().forEachRemaining(e -> e = null);
        begin = 0;
        end = 0;
        lastOp = LastOp.Remove;
    }

    private List<E> viewList() {
        if (end > begin || isEmpty())
            return elements.subList(begin, end);
        else {
            var first = elements.subList(begin, capacity);
            var second = elements.subList(0, end);
            first.addAll(second);
            return first;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return viewList().iterator();
    }

    @Override
    public boolean remove(Object arg0) {
        throw new UnsupportedOperationException("Cannot remove from ArrayQueue");
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        throw new UnsupportedOperationException("Cannot remove from ArrayQueue");
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        throw new UnsupportedOperationException("Cannot remove from ArrayQueue");
    }

    @Override
    public Object[] toArray() {
        return viewList().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return viewList().toArray(a);
    }
}
