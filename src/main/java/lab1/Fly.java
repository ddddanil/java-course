package lab1;

public class Fly implements Movement {
    public void move(Vector2D from, Vector2D dir) {
        from.add(dir.mult(2));
        System.out.println("Flying");
    }
}
