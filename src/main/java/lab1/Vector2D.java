package lab1;

public class Vector2D {
    public double x, y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D other) {
        x += other.x;
        y += other.y;
        return this;
    }

    public Vector2D mult(double k) {
        x *= k;
        y *= k;
        return this;
    }

    public String toString() {
        return String.format("Vector2D(%f, %f)", x, y);
    }
}
