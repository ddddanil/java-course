package lab1;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    static class Commands {
        static final Pattern pos = Pattern.compile("p(?:os)?");
        static final Pattern move = Pattern.compile("m(?:ove)?\s+(\\S+)\s+(\\S+)");
        static final Pattern strategy = Pattern.compile("s(?:trategy)?(?:\s+(\\w+))?");
        static final Pattern quit = Pattern.compile("q(?:uit)?");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Hero h = new Hero(new Vector2D(0, 0));
        System.out.println("(p)os, (m)ove, (s)trategy, (q)uit:");
        while (true) {
            try {
                System.out.print("> ");
                String cmd = sc.nextLine();
                Matcher pos_matcher = Commands.pos.matcher(cmd);
                Matcher move_matcher = Commands.move.matcher(cmd);
                Matcher strat_matcher = Commands.strategy.matcher(cmd);
                Matcher quit_matcher = Commands.quit.matcher(cmd);

                if (quit_matcher.matches()) {
                    System.out.println("Quitting");
                    break;
                } else if (pos_matcher.matches()) {
                    System.out.println(h.getPosition().toString());
                } else if (strat_matcher.matches()) {
                    if (strat_matcher.group(1) == null)
                        System.out.println(h.getStrategy().getClass().getSimpleName());
                    else {
                        String strat_name = strat_matcher.group(1);
                        switch (strat_name) {
                            case "walk" -> h.setStrategy(new Walk());
                            case "crawl" -> h.setStrategy(new Crawl());
                            case "fly" -> h.setStrategy(new Fly());
                            default -> throw new LabException("Unknown strategy");
                        }

                    }
                } else if (move_matcher.matches()) {
                    var x = Double.parseDouble(move_matcher.group(1));
                    var y = Double.parseDouble(move_matcher.group(2));
                    h.move(new Vector2D(x, y));
                }
                else {
                    System.out.println("No command matched");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
