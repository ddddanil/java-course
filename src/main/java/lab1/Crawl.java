package lab1;

public class Crawl implements Movement {
    public void move(Vector2D from, Vector2D dir) {
        from.add(dir.mult(0.5));
        System.out.println("Crawling");
    }
}
