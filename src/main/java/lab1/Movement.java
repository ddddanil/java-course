package lab1;

public interface Movement
{
    void move(Vector2D from, Vector2D dir);
}

