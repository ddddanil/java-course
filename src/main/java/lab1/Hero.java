package lab1;

public class Hero
{
    private Vector2D position;
    private Movement movement;

    public Hero(Vector2D pos) {
        position = pos;
        movement = null;
    }

    public Hero(Vector2D pos, Movement m) {
        position = pos;
        movement = m;
    }

    public void move(Vector2D dir) throws LabException {
        if(movement == null) {
            throw new LabException("No movement strategy");
        }
        movement.move(position, dir);
    }

    public Movement getStrategy() {
        return movement;
    }
    
    public void setStrategy(Movement m) throws LabException {
        if(m == null)
            throw new LabException("Cannot set movement to null");
        movement = m;
    }

    public Vector2D getPosition() {
        return position;
    }
}
