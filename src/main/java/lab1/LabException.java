package lab1;

public class LabException extends Exception {
    public LabException(String message) {
        super(message);
    }

    public LabException(String message, Throwable cause) {
        super(message, cause);
    }
}
