package backend;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.logging.Logger;

@SpringBootApplication
//@ComponentScan("backend.services")
//@EnableJpaRepositories("backend.repositories")
//@EntityScan("backend.entities")
public class WebApplication {
    private static ApplicationContext applicationContext;
    private static final Logger logger = Logger.getLogger(WebApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
//            String[] allBeanNames = applicationContext.getBeanDefinitionNames();
//            for(String beanName : allBeanNames) {
//                logger.fine(beanName);
//            }
        };
    }
}
