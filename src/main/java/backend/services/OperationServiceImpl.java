package backend.services;

import backend.dto.BalanceDto;
import backend.dto.NewOperation;
import backend.dto.OperationDto;
import backend.dto.TransactionDto;
import backend.entities.Operation;
import backend.exceptions.InvalidFilterException;
import backend.repositories.ArticleRepository;
import backend.repositories.OperationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class OperationServiceImpl implements OperationService {
    private final OperationRepository operations;
    private final ArticleRepository articles;
    private final BalanceService balances;

    public OperationServiceImpl(OperationRepository operations, ArticleRepository articleServiceImpl, BalanceService balanceServiceImpl) {
        this.operations = operations;
        this.articles = articleServiceImpl;
        this.balances = balanceServiceImpl;
    }

    public Stream<OperationDto> findAll() {
        return StreamSupport.stream(operations.findAll().spliterator(), false)
                .map(OperationDto::new);
    }

    public Optional<OperationDto> findById(Integer id) {
        return operations.findById(id).map(OperationDto::new);
    }

    public OperationDto register(NewOperation operation) {
        var balance = balances.findById(operation.getBalanceId()).orElseThrow();
        var transaction = new TransactionDto(operation);
        balances.update(balance, transaction);

        var article = articles.findById(operation.getArticleId()).orElseThrow();

        var op = new Operation(article, balance.toEntity(), operation.getDebit(), operation.getCredit());
        op = operations.save(op);
        return operations.findById(op.getId())
                .map(OperationDto::new).get();
    }

    public Boolean rollback(Integer operation_id) {
        var entity = operations.findById(operation_id);
        if (entity.isEmpty()) return false;
        var balance = entity.get().getBalance();
        var transaction = new TransactionDto(entity.get());
        transaction.setCredit(transaction.getCredit().negate());
        transaction.setDebit(transaction.getDebit().negate());
        balances.update(new BalanceDto(balance), transaction);

        operations.delete(entity.get());
        return true;
    }

    public Stream<OperationDto> findBy(Optional<String> article_name, Optional<Integer> article_id, Optional<Integer> balance_id) {
        if (article_name.isPresent() && article_id.isPresent()) {
            throw new InvalidFilterException("Filtering by both article id and article name");
        } else if (article_id.isPresent() && balance_id.isPresent()) {
            return operations.findByArticleIdAndBalanceId(article_id.get(), balance_id.get()).stream()
                    .map(OperationDto::new);
        } else if (article_name.isPresent() && balance_id.isPresent()) {
            return operations.findByArticleNameAndBalanceId(article_name.get(), balance_id.get()).stream()
                    .map(OperationDto::new);
        } else if (article_id.isPresent()) {
            return operations.findByArticleId(article_id.get()).stream()
                    .map(OperationDto::new);
        } else if (balance_id.isPresent()) {
            return operations.findByBalanceId(balance_id.get()).stream()
                    .map(OperationDto::new);
        } else if (article_name.isPresent()) {
            return operations.findByArticleName(article_name.get()).stream()
                    .map(OperationDto::new);
        } else {
            return findAll();
        }
    }

    public Stream<OperationDto> findByArticleId(Integer article_id) {
        return operations.findByArticleId(article_id).stream()
                .map(OperationDto::new);
    }
}
