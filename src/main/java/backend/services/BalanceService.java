package backend.services;

import backend.dto.BalanceDto;
import backend.dto.Comparison;
import backend.dto.NewArticleDto;
import backend.dto.TransactionDto;
import backend.entities.Article;
import backend.entities.Balance;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public interface BalanceService {
    Stream<BalanceDto> findAll();

    Optional<BalanceDto> findById(Integer id);

    BalanceDto create();

    BalanceDto update(BalanceDto base, TransactionDto update);

    Boolean delete(Integer balance_id);

    void pruneUnused();

    Stream<BalanceDto> findComparingAmount(BigDecimal amount, Comparison c);
}
