package backend.services;

import backend.dto.NewOperation;
import backend.dto.OperationDto;
import backend.entities.Balance;
import backend.entities.Operation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public interface OperationService {
    Stream<OperationDto> findAll();

    Optional<OperationDto> findById(Integer id);

    Stream<OperationDto> findByArticleId(Integer article_id);
    Stream<OperationDto> findBy(Optional<String> article_name, Optional<Integer> article_id, Optional<Integer> balance_id);

    OperationDto register(NewOperation operation);
    Boolean rollback(Integer operation_id);
}
