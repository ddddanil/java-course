package backend.services;

import backend.dto.ArticleDto;
import backend.dto.BalanceDto;
import backend.dto.NewArticleDto;
import backend.entities.Article;
import backend.entities.Balance;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Service
public interface ArticleService {
    Stream<ArticleDto> findAll();
    Optional<ArticleDto> findById(Integer id);
    Optional<ArticleDto> findByName(String name);

    ArticleDto create(NewArticleDto article);

    ArticleDto updateName(ArticleDto base, NewArticleDto update);

    Boolean delete(Integer article_id);
    void pruneUnused();

    Stream<BigDecimal> priceHistory(Integer article_id);
    Set<BalanceDto> boughtFrom(Integer article_id);
}
