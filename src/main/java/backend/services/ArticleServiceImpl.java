package backend.services;

import backend.dto.ArticleDto;
import backend.dto.BalanceDto;
import backend.dto.NewArticleDto;
import backend.dto.OperationDto;
import backend.repositories.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleRepository articles;
    @Autowired
    private OperationService operationService;
    @Autowired
    private BalanceService balanceService;

    public Stream<ArticleDto> findAll() {
        return StreamSupport.stream(articles.findAll().spliterator(), false)
                .map(ArticleDto::new);
    }

    public Optional<ArticleDto> findById(Integer id) {
        return articles.findById(id).map(ArticleDto::new);
    }

    public Optional<ArticleDto> findByName(String name) {
        return articles.findByName(name).map(ArticleDto::new);
    }

    public ArticleDto create(NewArticleDto article) {
        var articleEntity = article.toEntity();
        articleEntity = articles.save(articleEntity);
        return new ArticleDto(articleEntity);
    }

    public ArticleDto updateName(ArticleDto base, NewArticleDto update) {
        base.setName(update.getName());
        var entity = base.toEntity();
        entity = articles.save(entity);
        return new ArticleDto(entity);
    }

    public Boolean delete(Integer article_id) {
        try {
            articles.deleteById(article_id);
            return true;
        } catch (EmptyResultDataAccessException notFound) {
            return false;
        }
    }

    public void pruneUnused() {
        articles.pruneUnused();
    }

    public Stream<BigDecimal> priceHistory(Integer article_id) {
        try(var operations = operationService.findByArticleId(article_id)) {
            return operations.map(OperationDto::getAmount);
        }
    }

    public Set<BalanceDto> boughtFrom(Integer article_id) {
        try(var operations = operationService.findByArticleId(article_id)) {
            return operations.map(OperationDto::getBalance_id)
                    .map(id -> balanceService.findById(id))
                    .flatMap(Optional::stream)
                    .collect(Collectors.toSet());
        }
    }
}
