package backend.services;

import backend.dto.BalanceDto;
import backend.dto.Comparison;
import backend.dto.TransactionDto;
import backend.entities.Balance;
import backend.repositories.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Service
public class BalanceServiceImpl implements BalanceService {
    private final BalanceRepository balances;

    @Autowired
    public BalanceServiceImpl(BalanceRepository balances) {
        this.balances = balances;
    }

    public Stream<BalanceDto> findAll() {
        return StreamSupport.stream(balances.findAll().spliterator(), false)
                .map(BalanceDto::new);
    }

    public Optional<BalanceDto> findById(Integer id) {
        return balances.findById(id).map(BalanceDto::new);
    }

    public BalanceDto create() {
        var balance = new Balance(new BigDecimal(0), new BigDecimal(0));
        balance = balances.save(balance);
        return new BalanceDto(balance);
    }

    public BalanceDto update(BalanceDto base, TransactionDto update) {
        var entity = base.toEntity();
        update.addToTransaction(entity);
        entity = balances.save(entity);
        return new BalanceDto(entity);
    }

    public Boolean delete(Integer balance_id) {
        try {
            balances.deleteById(balance_id);
            return true;
        } catch (EmptyResultDataAccessException notFound) {
            return false;
        }
    }

    public void pruneUnused() {
        balances.pruneUnused();
    }

    public Stream<BalanceDto> findComparingAmount(BigDecimal amount, Comparison c) {
        Function<BigDecimal, List<Balance>> query = switch (c) {
            case LessThan -> balances::findByAmountLessThan;
            case LessOrEqual -> balances::findByAmountLessThanEqual;
            case GreaterThan -> balances::findByAmountGreaterThan;
            case GreaterOrEqual -> balances::findByAmountGreaterThanEqual;
        };
        return query.apply(amount).stream().map(BalanceDto::new);
    }
}
