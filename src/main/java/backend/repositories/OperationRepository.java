package backend.repositories;

import backend.entities.Article;
import backend.entities.Balance;
import backend.entities.Operation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.expression.spel.ast.OpPlus;
import org.springframework.lang.NonNullApi;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Stream;

@Repository
@Component
public interface OperationRepository extends CrudRepository<Operation, Integer> {
    @Override
    @Query("SELECT o FROM #{#entityName} o WHERE o.balance.deleted_at IS NOT NULL")
    Iterable<Operation> findAll();

    List<Operation> findByArticleId(Integer article_id);
    List<Operation> findByArticleName(@NotEmpty() @Size(min = 1, max = 50) String article_name);

    List<Operation> findByBalanceId(Integer balance_id);

    List<Operation> findByArticleIdAndBalanceId(Integer article_id, Integer balance_id);
    List<Operation> findByArticleNameAndBalanceId(@NotEmpty() @Size(min = 1, max = 50) String article_name, Integer balance_id);
}
