package backend.repositories;

import backend.entities.Article;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Component
public interface ArticleRepository extends CrudRepository<Article, Integer> {
    Optional<Article> findByName(String name);

    @Modifying
    @Transactional
    @Query("DELETE FROM Article a WHERE a.id NOT IN (SELECT o.article.id FROM Operation o GROUP BY o.article)")
    void pruneUnused();
}
