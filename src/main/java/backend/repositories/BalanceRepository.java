package backend.repositories;

import backend.entities.Balance;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

@Repository
@Component
public interface BalanceRepository extends CrudRepository<Balance, Integer> {
    @Transactional
    List<Balance> findByAmountGreaterThan(BigDecimal amount);

    @Transactional
    List<Balance> findByAmountGreaterThanEqual(BigDecimal amount);

    @Transactional
    List<Balance> findByAmountLessThan(BigDecimal amount);

    @Transactional
    List<Balance> findByAmountLessThanEqual(BigDecimal amount);

    @Modifying
    @Transactional
    @Query("DELETE FROM Balance b WHERE b.id NOT IN (SELECT o.balance.id FROM Operation o GROUP BY o.balance)")
    void pruneUnused();
}
