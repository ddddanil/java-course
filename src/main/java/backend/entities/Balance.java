package backend.entities;

import backend.dto.Transaction;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.*;
import org.springframework.data.annotation.CreatedDate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "Balance")
@Table(name = "balance")
@SQLDelete(sql = "UPDATE java_course.public.balance SET deleted_at = now() WHERE id = ?")
@Where(clause = "deleted_at IS NULL")
public class Balance implements java.io.Serializable, Transaction {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    public Balance(BigDecimal debit, BigDecimal credit) {
        this.debit = debit;
        this.credit = credit;
        this.amount = debit.subtract(credit);
        this.deleted_at = null;
    }

    @Basic(optional = false)
    @CreatedDate
    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime created_at = LocalDateTime.now();

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updated_at = LocalDateTime.now();

    @PreUpdate
    public void setUpdatedAt() {
        updated_at = LocalDateTime.now();
    }

    @Column(name = "deleted_at")
    private LocalDateTime deleted_at = null;

    @Column(name = "debit", nullable = false, precision = 18, scale = 2)
    public BigDecimal debit;

    @Column(name = "credit", nullable = false, precision = 18, scale = 2)
    public BigDecimal credit;

    @Generated(GenerationTime.ALWAYS)
    @Column(name = "amount", nullable = false, precision = 18, scale = 2,
            columnDefinition = "numeric(18, 2) GENERATED ALWAYS AS (debit - credit) STORED")
    private BigDecimal amount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Balance balance = (Balance) o;
        return id != null && Objects.equals(id, balance.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
