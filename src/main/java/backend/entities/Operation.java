package backend.entities;

import backend.dto.Transaction;
import javax.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity(name = "Operation")
@Table(name = "operations")
public class Operation implements java.io.Serializable, Transaction {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    public Operation(Article article, Balance balance, BigDecimal debit, BigDecimal credit) {
        this.article = article;
        this.balance = balance;
        this.debit = debit;
        this.credit = credit;
    }

    @ManyToOne
    @JoinColumn(name = "article_id")
    private Article article;

    @Basic(optional = false)
    @CreatedDate
    @CreationTimestamp
    @Column(name = "created_at", nullable = false)
    private LocalDateTime created_at = LocalDateTime.now();

    @UpdateTimestamp
    @Column(name = "updated_at", nullable = false)
    private LocalDateTime updated_at = LocalDateTime.now();

    @PreUpdate
    public void setUpdatedAt() {
        updated_at = LocalDateTime.now();
    }

    @ManyToOne
    @JoinColumn(name = "balance_id")
    private Balance balance;

    @Column(name = "debit", nullable = false, precision = 18, scale = 2)
    public BigDecimal debit;

    @Column(name = "credit", nullable = false, precision = 18, scale = 2)
    public BigDecimal credit;

    @Generated(GenerationTime.ALWAYS)
    @Column(name = "amount", nullable = false, precision = 18, scale = 2,
            columnDefinition = "numeric(18, 2) GENERATED ALWAYS AS (debit - credit) STORED")
    private BigDecimal amount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Operation operation = (Operation) o;
        return id != null && Objects.equals(id, operation.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
