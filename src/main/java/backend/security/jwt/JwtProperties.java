package backend.security.jwt;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "jwt")
public class JwtProperties {
    private long validatyInMs = 5 * 60 * 60 * 1000;

    @Value("${jwt.secret}")
    private String secretKey;
}