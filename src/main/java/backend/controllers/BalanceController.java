package backend.controllers;

import backend.dto.BalanceDto;
import backend.dto.Comparison;
import backend.services.BalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.math.BigDecimal;

@RestController
@RequestMapping("/balances")
public class BalanceController {
    @Autowired
    private BalanceService balanceService;

    @GetMapping()
    public ResponseEntity<Iterable<BalanceDto>> findAll() {
        var articles = balanceService.findAll().toList();
        return new ResponseEntity<>(articles, HttpStatus.OK);
    }

    @PostMapping(value = "/new", consumes = "application/json", produces = "application/json")
    public ResponseEntity<BalanceDto> create() {
        var balance = balanceService.create();
        return new ResponseEntity<>(balance, HttpStatus.CREATED);
    }

    @GetMapping("/compared")
    public ResponseEntity<Iterable<BalanceDto>> findByComparison(@RequestParam BigDecimal amount, @RequestParam Comparison compare) {
        var balances = balanceService.findComparingAmount(amount, compare).toList();
        return new ResponseEntity<>(balances, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BalanceDto> findById(@PathVariable("id") Integer id) {
        var balance = balanceService.findById(id);
        return balance.map(a -> new ResponseEntity<>(a, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id) {
        var deleted = balanceService.delete(id);
        return new ResponseEntity<>(deleted ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/prune")
    public ResponseEntity<?> pruneUnused() {
        balanceService.pruneUnused();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
