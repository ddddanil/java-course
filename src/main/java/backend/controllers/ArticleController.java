package backend.controllers;

import backend.dto.ArticleDto;
import backend.dto.BalanceDto;
import backend.dto.NewArticleDto;
import backend.entities.Article;
import backend.services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.security.RolesAllowed;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @GetMapping
    public ResponseEntity<Iterable<ArticleDto>> findAll() {
        var articles = articleService.findAll().toList();
        return new ResponseEntity<>(articles, HttpStatus.OK);
    }

    @GetMapping("/find")
    public ResponseEntity<ArticleDto> findBy(@RequestParam String title) {
        var article = articleService.findByName(title);
        return article.map(a -> new ResponseEntity<>(a, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "/new", consumes = "application/json", produces = "application/json")
    public ResponseEntity<ArticleDto> create(@RequestBody NewArticleDto articleDto) {
        var article = articleService.create(articleDto);
        return new ResponseEntity<>(article, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ArticleDto> findById(@PathVariable("id") Integer id) {
        var article = articleService.findById(id);
        return article.map(a -> new ResponseEntity<>(a, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ArticleDto> updateById(@PathVariable("id") Integer id, @RequestBody NewArticleDto updated) {
        var article = articleService.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        var resp = articleService.updateName(article, updated);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Integer id) {
        var deleted = articleService.delete(id);
        return new ResponseEntity<>(deleted ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/prune")
    public ResponseEntity<?> pruneUnused() {
        articleService.pruneUnused(); //.toList();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<Iterable<BigDecimal>> priceHistory(@PathVariable("id") Integer id) {
        var articleId = articleService.findById(id)
                .map(ArticleDto::getId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        var prices = articleService.priceHistory(articleId).toList();
        return new ResponseEntity<>(prices, HttpStatus.OK);
    }

    @GetMapping("/{id}/buyers")
    public ResponseEntity<Iterable<BalanceDto>> buyers(@PathVariable("id") Integer id) {
        var articleId = articleService.findById(id)
                .map(ArticleDto::getId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        var prices = articleService.boughtFrom(articleId);
        return new ResponseEntity<>(prices, HttpStatus.OK);
    }
}
