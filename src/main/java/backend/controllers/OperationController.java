package backend.controllers;

import backend.dto.ArticleDto;
import backend.dto.NewOperation;
import backend.dto.OperationDto;
import backend.exceptions.InvalidFilterException;
import backend.services.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Optional;

@RestController
@RequestMapping("/operations")
public class OperationController {
    @Autowired
    private OperationService operationService;

    @GetMapping
    public ResponseEntity<Iterable<OperationDto>> findAll() {
        var operations = operationService.findAll().toList();
        return new ResponseEntity<>(operations, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OperationDto> findById(@PathVariable Integer id) {
        var operation = operationService.findById(id);
        return operation.map(a -> new ResponseEntity<>(a, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/find")
    public ResponseEntity<Iterable<OperationDto>> findFilter(@RequestParam Optional<String> article_name, @RequestParam Optional<Integer> article_id, @RequestParam Optional<Integer> balance_id) {
        var operations = operationService.findBy(article_name, article_id, balance_id).toList();
        return new ResponseEntity<>(operations, HttpStatus.OK);
    }

    @PostMapping(value = "/register", consumes = "application/json", produces = "application/json")
    public ResponseEntity<OperationDto> register(@RequestBody NewOperation operation) {
        var entity = operationService.register(operation);
        return new ResponseEntity<>(entity, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}/rollback")
    public ResponseEntity<?> rollback(@PathVariable Integer id) {
        var rolledback = operationService.rollback(id);
        return new ResponseEntity<>(rolledback? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
