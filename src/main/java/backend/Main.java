package backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Main {
    static final String ConnectionUrl = "jdbc:postgresql://localhost:5432/java_course";

    static Connection getConnection() {
        Properties props = new Properties();
        props.setProperty("user", "java_course");
        props.setProperty("password", "java");
        props.setProperty("ssl", "false");
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        try {
            return DriverManager.getConnection(ConnectionUrl, props);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static void makeQuery(Connection conn) {
        String testQuery = """
                SELECT o.create_date, o.debit, o.credit, a.name
                FROM operations o
                INNER JOIN articles a on a.id = o.article_id
                ;""";


        try (var statement = conn.createStatement();
             var result = statement.executeQuery(testQuery)
        ) {
            System.out.printf("%20s %10s %10s %20s\n", "Create date", "Debit", "Credit", "Description");
            while (result.next()) {
                System.out.printf("%20s %10s %10s %20s\n",
                        result.getTime("create_date"),
                        result.getInt("debit"),
                        result.getInt("credit"),
                        result.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] argv) {
        var conn = getConnection();
        if (conn != null) {
            try (conn) {
                makeQuery(conn);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
