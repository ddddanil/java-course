package backend.dto;

import backend.entities.Article;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;

@Data
@AllArgsConstructor
@JsonSerialize
public class NewArticleDto {
    private @With String name;

    public NewArticleDto() {}

    public NewArticleDto(Article article) {
        this.name = article.getName();
    }

    public Article toEntity() {
        var article = new Article();
        article.setName(this.name);
        return article;
    }
}
