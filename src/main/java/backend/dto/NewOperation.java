package backend.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@JsonSerialize
public class NewOperation implements Transaction {
    private @With Integer articleId;
    private @With Integer balanceId;
    private @With BigDecimal debit;
    private @With BigDecimal credit;

    public NewOperation() {}

    public BigDecimal getAmount() {
        return debit.subtract(credit);
    }
}
