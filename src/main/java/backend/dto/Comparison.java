package backend.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public enum Comparison {
    LessThan("<"),
    LessOrEqual("<="),
    GreaterThan(">"),
    GreaterOrEqual(">=");

    private final String symbol;

    Comparison(String symbol) {
        this.symbol = symbol;
    }

    public String Symbol() {
        return this.symbol;
    }

    public <T extends Comparable<? super T>> boolean Compare(T a, T b) {
        var comp = a.compareTo(b);
        return switch (this) {
            case LessThan -> comp < 0;
            case LessOrEqual -> comp <= 0;
            case GreaterThan -> comp > 0;
            case GreaterOrEqual -> comp >= 0;
        };
    }
}
