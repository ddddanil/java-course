package backend.dto;

import backend.entities.Balance;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@JsonSerialize
public class BalanceDto implements Transaction {
    private Integer id;
    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal amount;

    public BalanceDto() {}

    public BalanceDto(Balance balance) {
        this.id = balance.getId();
        this.debit = balance.debit;
        this.credit = balance.credit;
        this.amount = balance.getAmount();
    }

    public Balance toEntity() {
        var balance = new Balance();
        balance.setId(this.id);
        balance.setDebit(this.debit);
        balance.setCredit(this.credit);
        balance.setAmount(this.amount);
        return balance;
    }
}
