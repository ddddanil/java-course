package backend.dto;

import backend.entities.Article;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@JsonSerialize
public class ArticleDto {
    private Integer id;
    private LocalDateTime created_at;
    private String name;

    public ArticleDto() {}

    public ArticleDto(Article article) {
        this.id = article.getId();
        this.created_at = article.getCreated_at();
        this.name = article.getName();
    }

    public Article toEntity() {
        var article = new Article();
        article.setId(this.id);
        article.setCreated_at(this.created_at);
        article.setName(this.name);
        return article;
    }
}
