package backend.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class AuthRequest {
    private @With String username;
    private @With String password;
}

