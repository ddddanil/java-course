package backend.dto;

import backend.entities.User;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class AuthResponse {
    private @With String username;

    public AuthResponse(User user) {
        this.username = user.getUsername();
    }
}
