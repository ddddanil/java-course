package backend.dto;

import backend.entities.Operation;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@JsonSerialize
public class OperationDto implements Transaction {
    private Integer id;
    private Integer article_id;
    private Integer balance_id;

    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal amount;

    public OperationDto() {}

    public OperationDto(Operation operation) {
        this.id = operation.getId();
        this.article_id = operation.getArticle().getId();
        this.balance_id = operation.getBalance().getId();

        this.debit = operation.debit;
        this.credit = operation.credit;
        this.amount = operation.getAmount();
    }
}
