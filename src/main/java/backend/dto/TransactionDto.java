package backend.dto;

import backend.entities.Balance;
import backend.entities.Operation;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@JsonSerialize
public class TransactionDto implements Transaction {
    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal amount;

    public TransactionDto() {}

    public TransactionDto(BigDecimal debit, BigDecimal credit) {
        this.debit = debit;
        this.credit = credit;
    }

    public TransactionDto(Transaction operation) {
        this.debit = operation.getDebit();
        this.credit = operation.getCredit();
    }

    public<T extends Transaction> T setToTransaction(T t) {
        t.setDebit(this.debit);
        t.setCredit(this.credit);
        return t;
    }

    public<T extends Transaction> T addToTransaction(T t) {
        t.setDebit(t.getDebit().add(this.debit));
        t.setCredit(t.getCredit().add(this.credit));
        return t;
    }
}
