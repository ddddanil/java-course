package backend.dto;

import java.math.BigDecimal;

public interface Transaction {
    BigDecimal getDebit();
    void setDebit(BigDecimal debit);
    BigDecimal getCredit();
    void setCredit(BigDecimal credit);
    BigDecimal getAmount();
}
