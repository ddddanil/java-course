CREATE TABLE IF NOT EXISTS articles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50)
)
;

CREATE TABLE IF NOT EXISTS balance (
   id SERIAL PRIMARY KEY,
   create_date TIMESTAMPTZ,
   debit NUMERIC(18,2),
   credit NUMERIC(18,2),
   amount NUMERIC(18,2) GENERATED ALWAYS AS ( debit - credit ) STORED
)
;

CREATE TABLE IF NOT EXISTS operations (
    id SERIAL PRIMARY KEY,
    article_id INT4 NOT NULL,
    balance_id INT4 NOT NULL,
    create_date TIMESTAMPTZ,
    debit NUMERIC(18,2),
    credit NUMERIC(18,2),
    CONSTRAINT fk_article
      FOREIGN KEY (article_id)
          REFERENCES articles(id),
    CONSTRAINT fk_balance
      FOREIGN KEY (balance_id)
          REFERENCES balance(id)
)
;
