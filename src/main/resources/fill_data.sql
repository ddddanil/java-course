INSERT INTO articles (name)
VALUES
    ('T-Shirt'),
    ('Cereal'),
    ('Mortgage'),
    ('Mortgage payment')
RETURNING id
;

INSERT INTO balance (create_date, debit, credit)
VALUES
    (NOW(), 0, 0),
    (NOW(), 1000000, 1000000),
    (NOW(), 999940, 1000000),
    (NOW(), 998940, 999000)
RETURNING id
;

INSERT INTO operations (article_id, balance_id, create_date, debit, credit)
VALUES
    (3, 2, NOW(), 1000000, 1000000),
    (2, 3, NOW(), -60, 0),
    (4, 4, NOW(), -1000, -1000)
RETURNING id
;
